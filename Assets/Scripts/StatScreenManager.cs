﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class StatScreenManager : MonoBehaviour {

	public class PlayerSettings {
		public int _tankIndex;
		public int _sensitivityIndex;
		public float _sensitivityMultp;
		public bool _altControls;

		// Change default player settings here
		public PlayerSettings() {
			_tankIndex = (int)Tank.Type.Medium;
			_sensitivityIndex = 5;
			_sensitivityMultp = 0;
			_altControls = true;
		}
	}

	public class PlayerData {
		public string _playerName;

		public int _wins;
		public int _kills;
		public int _suicides;
		public int _shotsFired;
		public int _shotsHit;
		public float _accuracy;
		public float _damage;
		public float _selfDamage;

		public PlayerData(string name) {
			_playerName = name;
			_wins = 0;
			_kills = 0;
			_suicides = 0;
			_shotsFired = 0;
			_shotsHit = 0;
			_accuracy = 0f;
			_damage = 0f;
			_selfDamage = 0f;
		}

		public PlayerData(string name, int wins, int kills, int suicides, int shots, int shotsHit, float acc, float dmg, float selfDmg) {
			_playerName = name;
			_wins = wins;
			_kills = kills;
			_suicides = suicides;
			_shotsFired = shots;
			_shotsHit = shotsHit;
			_accuracy = acc;
			_damage = dmg;
			_selfDamage = selfDmg;
		}
	}

	public static StatScreenManager instance;

	public bool _debugMode = false;
	public TextMeshProUGUI _roundText;
	public TextMeshProUGUI _countdownText;
	public Transform _playerPanelParent;
	public Transform _statPanel;
	private RectTransform _statPanelRect;
	private Image _blackImage;

	private bool _screenActive = false;

	public PlayerSettings[] _playerSettings = new PlayerSettings[GameManager._playerWrappers.Length];
	public PlayerData[] _playerData = new PlayerData[GameManager._playerWrappers.Length];
	private RectTransform[] _playerPanels = new RectTransform[GameManager._playerWrappers.Length];
	private bool[] _activePlayers = new bool[GameManager._playerWrappers.Length];
	private bool[] _readyPlayers = new bool[GameManager._playerWrappers.Length];
	private bool _allReady = false;
	private float _maxCountDown = 3f;
	private float _countdownTimer;

	private int[] _selectionIndexArray = new int[GameManager._playerWrappers.Length];
	private int _sensitivityIndexMax = 12;
	private int _sensitivityPerIndex = 20;
	private int _sensivityBase = 60;

	private float _repeatDelay = 0.25f;
	private float[] _repeatDelayTimer = new float[GameManager._playerWrappers.Length];
	private float[] _repeatTimer = new float[GameManager._playerWrappers.Length];

	private int _victoryCondition = 5;
	private int _maxVictoryCondition = 15;

	void Awake() {
		instance = this;
		transform.GetComponent<Canvas>().enabled = false;
		_statPanelRect = _statPanel.GetComponent<RectTransform>();
		_blackImage = transform.GetChild(0).GetComponent<Image>();
		// Dublicate player menus
		for (int i = 0; i < 3; i++) {
			Instantiate(_playerPanelParent.GetChild(0), _playerPanelParent);
		}
	}
	
	void Start() {
		for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
			_playerPanels[i] = _playerPanelParent.GetChild(i).GetComponent<RectTransform>();
			_playerPanels[i].GetChild(2).GetComponent<Image>().color = GameManager.instance._playerColors[i];
			Color alphaColor = GameManager.instance._playerColors[i];
			alphaColor.a = 0.3f;
			_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetComponent<Image>().color = alphaColor;
			_statPanel.GetChild(0).GetChild(2).GetChild(i).GetComponent<Image>().color = alphaColor;
			_playerSettings[i] = new PlayerSettings();
			_playerData[i] = new PlayerData("Player " + (i + 1));
			UpdatePlayerSettings(i);
		}
		for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
			UpdateTankStats(i);
		}
	}

	void Update() {
		if (_screenActive == true) {
			List<int> activeControllers = new List<int>();
			bool openSlots = false;
			for (int j = 0; j < GameManager._playerWrappers.Length; j++) {
				if (GameManager._playerWrappers[j] == null) {
					openSlots = true;
				}
				else {
					activeControllers.Add(GameManager._playerWrappers[j]._controllerIndex);
				}
			}
			// Read inputs for joined players
			for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
				if (GameManager._playerWrappers[i] != null) {
					if (_readyPlayers[i] == false) {
						// Joystick at rest, reset timers
						if (Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Vertical") < GameManager._joystickDeadZone &&
							Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Vertical") > -GameManager._joystickDeadZone &&
							Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Horizontal") < GameManager._joystickDeadZone &&
							Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Horizontal") > -GameManager._joystickDeadZone) {
							_repeatDelayTimer[i] = _repeatDelay;
							_repeatTimer[i] = _repeatDelay;
						}
						else {
							_repeatTimer[i] += Time.unscaledDeltaTime;
							_repeatDelayTimer[i] -= 0.20f * Time.unscaledDeltaTime;
							if (_repeatDelayTimer[i] < 0.04f) {
								_repeatDelayTimer[i] = 0.04f;
							}
						}
						if (Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Vertical") > GameManager._joystickDeadZone && _repeatTimer[i] > _repeatDelayTimer[i]) {
							MoveUp(i);
						}
						else if (Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Vertical") < -GameManager._joystickDeadZone && _repeatTimer[i] > _repeatDelayTimer[i]) {
							MoveDown(i);
						}
						else if (Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Horizontal") > GameManager._joystickDeadZone && _repeatTimer[i] > _repeatDelayTimer[i]) {
							MoveRight(i);
						}
						else if (Input.GetAxisRaw("C" + GameManager._playerWrappers[i]._controllerIndex + "Horizontal") < -GameManager._joystickDeadZone && _repeatTimer[i] > _repeatDelayTimer[i]) {
							MoveLeft(i);
						}

						if (_repeatTimer[i] > _repeatDelayTimer[i]) {
							_repeatTimer[i] = 0;
						}
					}
					// Ready up
					if (Input.GetButtonDown("C" + GameManager._playerWrappers[i]._controllerIndex + "Submit")) {
						if (_readyPlayers[i] == false) {
							ReadyPlayer(i, true);
						}
						CheckAllReady();
					}
					// Leave game
					if (Input.GetButtonDown("C" + GameManager._playerWrappers[i]._controllerIndex + "Cancel") && _countdownTimer > 0f) {
						if (_readyPlayers[i] == false) {
							_activePlayers[i] = false;
							GameManager._playerWrappers[i] = null;
							DeactivatePlayerMenu(i);
						}
						else {
							ReadyPlayer(i, false);
						}
						CheckAllReady();
					}
				}
			}
			// Add new players
			for (int i = 0; i < GameManager._maxControllers; i++) {
				if (openSlots == true) {
					if (activeControllers.Contains(i) == false) {
						if (Input.GetButtonDown("C" + i + "Submit")) {
							// Insert player into first empty slot
							for (int j = 0; j < GameManager._playerWrappers.Length; j++) {
								if (GameManager._playerWrappers[j] == null) {
									_activePlayers[j] = true;
									GameManager._playerWrappers[j] = new GameManager.PlayerWrapper(i);
									//_playerData[j] = new PlayerData("Player " + (i + 1));
									ActivatePlayerMenu(j);
									CheckAllReady();
									break;
								}
							}
						}
					}
				}
			}
			// Game start countdown
			if (_allReady == true) {
				_countdownText.gameObject.SetActive(true);
				int previous = Mathf.CeilToInt(_countdownTimer);
				_countdownTimer -= Time.unscaledDeltaTime * 1.5f;
				if (_countdownTimer > 0f) {
					_countdownText.text = Mathf.CeilToInt(_countdownTimer).ToString();
					if (previous > Mathf.CeilToInt(_countdownTimer)) {
						// Play countdown sound
					}
				}
				else {
					_screenActive = false;
					StartGame();
				}
			}
			else {
				_countdownText.gameObject.SetActive(false);
			}
		}
	}

	public void ShowGameStartScreen() {
		Input.ResetInputAxes();
		_screenActive = true;
		transform.GetComponent<Canvas>().enabled = true;
		_blackImage.color = new Color(0f, 0f, 0f, 0.3125f);
		_playerPanelParent.parent.GetComponent<Canvas>().enabled = true;
		_statPanel.gameObject.SetActive(false);
		_roundText.text = "";
		_countdownTimer = _maxCountDown;
		for (int i = 0; i < _playerPanelParent.childCount; i++) {
			DeactivatePlayerMenu(i);
		}
	}

	public IEnumerator ShowRoundScoreScreen() {
		transform.GetComponent<Canvas>().enabled = true;
		_blackImage.color = new Color(0f, 0f, 0f, 0.3125f * 2f);
		_playerPanelParent.parent.GetComponent<Canvas>().enabled = false;
		_statPanel.gameObject.SetActive(true);
		_statPanelRect.pivot = new Vector2(0.5f, 0.5f);
		_statPanelRect.anchorMax = new Vector2(0.5f, 0.5f);
		_statPanelRect.anchorMin = new Vector2(0.5f, 0.5f);
		_statPanelRect.anchoredPosition = new Vector2(0f, 0f);
		_roundText.text = "Round " + GameManager.instance._gameRound;
		_countdownText.gameObject.SetActive(false);

		int wins = 0;
		bool suicide = false;
		for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
			if (GameManager._playerWrappers[i] != null) {
				if (GameManager._playerWrappers[i]._player._kills > wins) {
					wins = GameManager._playerWrappers[i]._player._kills;
				}
				if (GameManager._playerWrappers[i]._player._suicide == 1) {
					suicide = true;
				}
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(1).GetComponent<TextMeshProUGUI>().text = _playerData[i]._playerName;
				_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text = _playerData[i]._playerName;
				for (int j = 0; j < _maxVictoryCondition; j++) {
					if (j < _playerData[i]._kills - _playerData[i]._suicides) {
						_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(j + 2).GetComponent<Animator>().Play("On");
					}
					else {
						_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(j + 2).GetComponent<Animator>().Play("Off");
					}
					if (j >= _victoryCondition) {
						_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(j + 2).gameObject.SetActive(false);
					}
				}
			}
			else {
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(1).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text = "-";
				for (int j = 0; j < _maxVictoryCondition; j++) {
					_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(j + 2).gameObject.SetActive(false);
				}
			}
		}

		yield return new WaitForSeconds(0.5f);
		for (int j = 0; j < wins; j++) {
			for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
				if (GameManager._playerWrappers[i] != null) {
					if (GameManager._playerWrappers[i]._player._kills > 0 && j <= GameManager._playerWrappers[i]._player._kills) {
						_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(_playerData[i]._kills - _playerData[i]._suicides + j + 2).GetComponent<Animator>().Play("Activate");
					}
				}
			}
			yield return new WaitForSeconds(0.4f);
		}
		if (suicide == true) {
			for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
				if (GameManager._playerWrappers[i] != null) {
					if (GameManager._playerWrappers[i]._player._suicide == 1) {
						_statPanel.GetChild(0).GetChild(2).GetChild(i).GetChild(_playerData[i]._kills - _playerData[i]._suicides + GameManager._playerWrappers[i]._player._kills + 1).GetComponent<Animator>().Play("Suicide");
					}
				}
			}
			yield return new WaitForSeconds(0.4f);
		}
	}

	public void ShowMatchScreen() {
		Input.ResetInputAxes();
		_screenActive = true;
		transform.GetComponent<Canvas>().enabled = true;
		_blackImage.color = new Color(0f, 0f, 0f, 0.3125f);
		_playerPanelParent.parent.GetComponent<Canvas>().enabled = true;
		_statPanel.gameObject.SetActive(true);
		_statPanelRect.pivot = new Vector2(0.5f, 1f);
		_statPanelRect.anchorMax = new Vector2(0.5f, 1f);
		_statPanelRect.anchorMin = new Vector2(0.5f, 1f);
		_statPanelRect.anchoredPosition = new Vector2(0f, 0f);
		_roundText.text = "Round " + GameManager.instance._gameRound;
		_countdownTimer = _maxCountDown;
		CheckAllReady();
		// Set stats on screen
		for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
			if (GameManager._playerWrappers[i] != null) {
				_playerData[i]._wins += GameManager._playerWrappers[i]._player._win;
				_playerData[i]._kills += GameManager._playerWrappers[i]._player._kills;
				_playerData[i]._shotsFired += GameManager._playerWrappers[i]._player._shotsFired;
				_playerData[i]._shotsHit += GameManager._playerWrappers[i]._player._shotsHit;
				if (GameManager._playerWrappers[i]._player._shotsFired > 0) {
					_playerData[i]._accuracy = (1f * _playerData[i]._shotsHit / _playerData[i]._shotsFired);
				}
				_playerData[i]._damage += GameManager._playerWrappers[i]._player._damageDealt;
				_playerData[i]._selfDamage += GameManager._playerWrappers[i]._player._selfDamage;

				// Crown
				if (GameManager._playerWrappers[i]._player._win == 1) {
					_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(0).GetChild(0).gameObject.SetActive(true);
				}
				else {
					_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(0).GetChild(0).gameObject.SetActive(false);
				}
				// Skull
				if (GameManager._playerWrappers[i]._player._suicide == 1) {
					_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).gameObject.SetActive(true);
					_playerData[i]._suicides++;
				}
				else {
					_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).gameObject.SetActive(false);
				}
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(1).GetComponent<TextMeshProUGUI>().text = _playerData[i]._playerName;

				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(2).GetComponent<TextMeshProUGUI>().text = _playerData[i]._wins.ToString();
				
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(3).GetComponent<TextMeshProUGUI>().text =
					GameManager._playerWrappers[i]._player._kills + " / " + _playerData[i]._kills;
				
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(4).GetComponent<TextMeshProUGUI>().text =
					GameManager._playerWrappers[i]._player._shotsFired + " / " + _playerData[i]._shotsFired;
				
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(5).GetComponent<TextMeshProUGUI>().text =
					(GameManager._playerWrappers[i]._player._accuracy * 100f).ToString("0") + "% / " + (_playerData[i]._accuracy * 100f).ToString("0") + "%";
				
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(6).GetComponent<TextMeshProUGUI>().text =
					GameManager._playerWrappers[i]._player._damageDealt.ToString("0") + " / " + _playerData[i]._damage.ToString("0");
				
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(7).GetComponent<TextMeshProUGUI>().text =
					GameManager._playerWrappers[i]._player._selfDamage.ToString("0") + " / " + _playerData[i]._selfDamage.ToString("0");
			}
			else {
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(0).GetChild(0).gameObject.SetActive(false);
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).gameObject.SetActive(false);
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(1).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(2).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(3).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(4).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(5).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(6).GetComponent<TextMeshProUGUI>().text = "-";
				_statPanel.GetChild(0).GetChild(1).GetChild(i + 1).GetChild(7).GetComponent<TextMeshProUGUI>().text = "-";
			}
		}
	}

	private void ActivatePlayerMenu(int index) {
		_playerPanels[index].GetChild(0).GetChild(3).gameObject.SetActive(false);
		_playerPanels[index].pivot = new Vector2(0.5f, -0.1f);
		// Reset player settings
		SelectOption(index);
	}

	private void DeactivatePlayerMenu(int index) {
		_playerPanels[index].GetChild(0).GetChild(3).gameObject.SetActive(true);
		_playerPanels[index].pivot = new Vector2(0.5f, 1f);
	}

	private void MoveUp(int playerIndex) {
		_selectionIndexArray[playerIndex]--;
		if (_selectionIndexArray[playerIndex] < 0) {
			_selectionIndexArray[playerIndex] = 2;
		}
		SelectOption(playerIndex);
	}

	private void MoveDown(int playerIndex) {
		_selectionIndexArray[playerIndex]++;
		if (_selectionIndexArray[playerIndex] > 2) {
			_selectionIndexArray[playerIndex] = 0;
		}
		SelectOption(playerIndex);
	}

	private void MoveRight(int playerIndex) {
		ChangeOption(playerIndex, 1);
	}

	private void MoveLeft(int playerIndex) {
		ChangeOption(playerIndex, -1);
	}

	private void SelectOption(int playerIndex) {
		for (int i = 0; i < _playerPanels[playerIndex].childCount; i++) {
			if (i == _selectionIndexArray[playerIndex]) {
				_playerPanels[playerIndex].GetChild(2).GetChild(i).GetChild(2).gameObject.SetActive(true);
			}
			else {
				_playerPanels[playerIndex].GetChild(2).GetChild(i).GetChild(2).gameObject.SetActive(false);
			}
		}
	}

	private void ChangeOption(int playerIndex, int change) {
		// Tank
		if (_selectionIndexArray[playerIndex] == 0) {
			if (change > 0) {
				_playerSettings[playerIndex]._tankIndex++;
				if (_playerSettings[playerIndex]._tankIndex > System.Enum.GetValues(typeof(Tank.Type)).Length - 1) {
					_playerSettings[playerIndex]._tankIndex = (int)Tank.Type.Light;
				}
			}
			else {
				_playerSettings[playerIndex]._tankIndex--;
				if (_playerSettings[playerIndex]._tankIndex < 0) {
					_playerSettings[playerIndex]._tankIndex = (int)Tank.Type.Heavy;
				}
			}
			UpdateTankStats(playerIndex);
		}
		// Sensitivity
		else if (_selectionIndexArray[playerIndex] == 1) {
			if (change > 0) {
				_playerSettings[playerIndex]._sensitivityIndex++;
				if (_playerSettings[playerIndex]._sensitivityIndex > _sensitivityIndexMax) {
					_playerSettings[playerIndex]._sensitivityIndex = 0;
				}
			}
			else {
				_playerSettings[playerIndex]._sensitivityIndex--;
				if (_playerSettings[playerIndex]._sensitivityIndex < 0) {
					_playerSettings[playerIndex]._sensitivityIndex = _sensitivityIndexMax;
				}
			}
		}
		// Controls
		else {
			_playerSettings[playerIndex]._altControls = !_playerSettings[playerIndex]._altControls;
		}
		UpdatePlayerSettings(playerIndex);
	}

	private void UpdatePlayerSettings(int playerIndex) {
		_playerPanels[playerIndex].GetChild(2).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = System.Enum.GetName(typeof(Tank.Type), _playerSettings[playerIndex]._tankIndex).ToString();
		_playerPanels[playerIndex].GetChild(2).GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>().text = (_sensivityBase + _playerSettings[playerIndex]._sensitivityIndex * _sensitivityPerIndex) + "%";
		if (_playerSettings[playerIndex]._altControls == false) {
			_playerPanels[playerIndex].GetChild(2).GetChild(2).GetChild(1).GetComponent<TextMeshProUGUI>().text = "Tank";
		}
		else {
			_playerPanels[playerIndex].GetChild(2).GetChild(2).GetChild(1).GetComponent<TextMeshProUGUI>().text = "Arcade";
		}
	}

	private void UpdateTankStats(int playerIndex) {
		float baseValueMultiplier = 0.5F;
		float minDamage = TankStats.GetMinStat(TankStats.StatType.Damage);
		float maxDamage = TankStats.GetMaxStat(TankStats.StatType.Damage);
		float damage = TankStats.tankStats[StatScreenManager.instance._playerSettings[playerIndex]._tankIndex].damage;
		float damageBase = minDamage * baseValueMultiplier;
		float damageFill = (damageBase + damage - minDamage) / (maxDamage - minDamage + damageBase);
		_playerPanels[playerIndex].GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = damageFill;
		
		float minSpeed = TankStats.GetMinStat(TankStats.StatType.Speed);
		float maxSpeed = TankStats.GetMaxStat(TankStats.StatType.Speed);
		float speed = TankStats.tankStats[StatScreenManager.instance._playerSettings[playerIndex]._tankIndex].speed;
		float speedBase = minSpeed * baseValueMultiplier;
		float speedFill = (speedBase + speed - minSpeed) / (maxSpeed - minSpeed + speedBase);
		_playerPanels[playerIndex].GetChild(0).GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = speedFill;
		
		float minHitPoints = TankStats.GetMinStat(TankStats.StatType.HitPoints);
		float maxHitPoints = TankStats.GetMaxStat(TankStats.StatType.HitPoints);
		float hitPoints = TankStats.tankStats[StatScreenManager.instance._playerSettings[playerIndex]._tankIndex].hitPoints;
		float hitPointsBase = minHitPoints * baseValueMultiplier;
		float hitPointsFill = (hitPointsBase + hitPoints - minHitPoints) / (maxHitPoints - minHitPoints + hitPointsBase);
		_playerPanels[playerIndex].GetChild(0).GetChild(2).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = hitPointsFill;
		
		float minTurbo = TankStats.GetMinStat(TankStats.StatType.Turbo);
		float maxTurbo = TankStats.GetMaxStat(TankStats.StatType.Turbo);
		float turbo = TankStats.tankStats[StatScreenManager.instance._playerSettings[playerIndex]._tankIndex].turbo;
		float turboBase = minTurbo * baseValueMultiplier;
		float turboFill = (turboBase + turbo - minTurbo) / (maxTurbo - minTurbo + turboBase);
		_playerPanels[playerIndex].GetChild(0).GetChild(2).GetChild(3).GetChild(0).GetComponent<Image>().fillAmount = turboFill;
	}

	private void ReadyPlayer(int playerIndex, bool b) {
		if (b == true) {
			_playerPanels[playerIndex].GetChild(2).gameObject.SetActive(false);
		}
		else {
			_playerPanels[playerIndex].GetChild(2).gameObject.SetActive(true);
		}
		_readyPlayers[playerIndex] = b;
	}

	private void CheckAllReady() {
		int readyPlayers = 0;
		bool allReady = true;
		for (int i = 0; i < _activePlayers.Length; i++) {
			if (_activePlayers[i] == true) {
				if (_readyPlayers[i] == true) {
					readyPlayers++;
				}
				else {
					allReady = false;
				}
			}
		}
		if (readyPlayers == 0) {
			allReady = false;
		}

		if ((readyPlayers > 1 || _debugMode == true) && allReady) {
			_allReady = true;
		}
		else {
			_allReady = false;
			_countdownTimer = _maxCountDown;
		}
	}

	private void StartGame() {
		_allReady = false;
		for (int i = 0; i < _readyPlayers.Length; i++) {
			if (_readyPlayers[i] == true) {
				ReadyPlayer(i, false);
			}
		}
		for (int i = 0; i < _playerSettings.Length; i++) {
			_playerSettings[i]._sensitivityMultp = (_sensivityBase + _sensitivityPerIndex * _playerSettings[i]._sensitivityIndex) / 100f;
		}
		transform.GetComponent<Canvas>().enabled = false;
		GameManager.instance.StartLocalRound();
	}
}
