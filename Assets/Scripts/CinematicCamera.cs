﻿using UnityEngine;
using System.Collections;

public class CinematicCamera : MonoBehaviour {

	public static CinematicCamera instance;

	public Vector3 _levelCenter;
	public float _cameraDistance;
	public float _cameraHeight;
	public float _rotateSpeed;

	public bool _showCamera = true;

	private Camera _camera;

	void Awake() {
		instance = this;
		_camera = GetComponent<Camera>();
	}
	
	void Update() {
		transform.RotateAround(_levelCenter, Vector3.up, -_rotateSpeed * Time.unscaledDeltaTime);
		transform.LookAt(_levelCenter);
	}

	public void SetupTracking(Vector3 levelCenter, float distance, float height) {
		_levelCenter = levelCenter;
		_cameraDistance = distance;
		_cameraHeight = height;

		transform.position = _levelCenter + new Vector3(distance, height, 0f);
	}

	public void ShowCamera(bool b) {
		_camera.enabled = b;
	}
}
