﻿using UnityEngine;
//using System.Collections;

public class ExplosionSphere : MonoBehaviour {

	public AnimationCurve _sizeCurve;
	public float _lifetime = 0.5f;
	public ParticleSystem[] _particles;

	private float _timer = 0f;
	private float _explosionSize;
	private bool _active = false;

	private void Awake() {
		Disable();
	}

	private void Disable() {
		_active = false;
		transform.GetChild(0).localScale = Vector3.zero;
	}

	public void Enable(Vector3 pos, float size) {
		_active = true;
		transform.position = pos;
		_explosionSize = size;
		for (int i = 0; i < _particles.Length; i++) {
			_particles[i].transform.localScale = Vector3.one * _explosionSize / 4f;
			_particles[i].Play();
		}
		_timer = 0f;
	}
	
	void Update() {
		if (_active == true) {
			if (_timer < _lifetime) {
				_timer += Time.deltaTime;
				transform.GetChild(0).localScale = Vector3.one * _explosionSize * _sizeCurve.Evaluate(_timer / _lifetime);
			}
			else {
				Disable();
			}
		}
	}
}
