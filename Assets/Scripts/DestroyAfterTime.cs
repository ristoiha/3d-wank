﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

	public float _time;
	
	void Update() {
		_time -= Time.deltaTime;
		if (_time <= 0f) {
			Destroy(gameObject);
		}
	}
}
