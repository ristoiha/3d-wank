﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLevel : MonoBehaviour {

	public class SpawnPosition {
		public Vector3 _position;
		public float _facing;

		public SpawnPosition(Vector3 pos, float face) {
			_position = pos;
			_facing = face;
		}
	}

	private class Impact {
		public Vector2 pos;
		public float time;
		public float magnitude;
		public float fadeTime;
	}

	public Vector2 _worldSize;

	public AnimationCurve holeShape;
	public bool hills = true;

	public static CreateLevel instance;
	public SpawnPosition[] playerSpawnPositions;
	public Texture2D tex;
	public bool _randomizeSpawnPositions = true;
	private int xSize = 128;
	private int ySize = 128;
	private Vector3[] verts;
	private float[] heightMap;
	private Vector2[] uvs;
	private int[] tris;
	private float gridSpacing = 0.5F;
	private float waveSpeed = Mathf.PI * 10;
	private float waveLength = 2F;
	private List<Impact> impacts = new List<Impact>();
	private const float sqr2 = 1.4142135623731F;
	// private float cooldownTime = 2F;
	// private float cooldownTimer = 0F;
	private float offset;
	public Texture2D alphaTex;
	private Color[] colors;
	private float baseImpactFadeTime = 2F;
	public static bool levelCreated = false;
	
	void Awake() {
		instance = this;
		offset = transform.position.y;
	}
	
	public void GenerateLevel() {
		Random.InitState(GameManager.instance.sParams.randomSeed);
		InitializeHeightMap();
		InitializeMesh();
		InitializeAlphaMap();
		levelCreated = true;
		CinematicCamera.instance.SetupTracking(new Vector3(xSize / 4f, 4f, ySize / 4f), xSize / 4f, xSize / 10f);
		_worldSize = new Vector2(xSize / 2f, ySize / 2f);
	}
	
	void Update() {
		if (GameManager.instance.sParams.randomSeed != -1 && levelCreated == false) {
			GenerateLevel();
		}
		if (levelCreated == false) {
			return;
		}
		//cooldownTimer -= Time.deltaTime;
		//if (cooldownTimer < 0F) {
		//	if (Input.GetKeyDown(KeyCode.F)) {
		//		cooldownTimer = cooldownTime;
		//		AddImpact(new Vector2(Random.Range(0F, (float)xSize * gridSpacing), Random.Range(0F, (float)ySize * gridSpacing)), 0.2F);
		//	}
		//	for (int i = impacts.Count - 1; i > -1; i--) {
		//		if (Time.time - impacts[i].time > xSize / waveSpeed * sqr2 + 2F) {
		//			impacts.RemoveAt(i);
		//		}
		//	}
		float delta = Time.deltaTime;
		float curTime = Time.time;
		float distance = 0F;
		Impact impact;
		for (int i = impacts.Count - 1; i > -1; i--) {
			impact = impacts[i];
			for (int y = 0; y < ySize; y++) {
				for (int x = 0; x < xSize + 1; x++) {
					//distance = Vector2.Distance(impacts[i].pos, new Vector2(x, y));
					distance = new Vector2(impact.pos.x - x, impact.pos.y - y).magnitude;
					float baa = distance - (curTime - impact.time) * waveSpeed;
					float baa2 = (baa + delta * waveSpeed);
					if ((baa > -Mathf.PI * 2 * waveLength && baa < 0F) || (baa2 > -Mathf.PI * 2 * waveLength && baa2 < 0)) {
						float currentTimeValue = baa / waveLength;
						float previousTimeValue = baa2 / waveLength;
						if (baa2 > 0) {
							previousTimeValue = 0F;
						}
						if (baa < -Mathf.PI * 2 * waveLength) {
							currentTimeValue = 0F;
						}
						float increase = -Mathf.Cos(currentTimeValue) + Mathf.Cos(previousTimeValue);
						float hattuvakio = waveLength / waveSpeed * Mathf.PI;
						if (increase > 0F) {
							verts[y * (xSize + 1) + x].y += increase * impact.magnitude * impact.fadeTime / baseImpactFadeTime / waveLength;
						}
						else {
							verts[y * (xSize + 1) + x].y += increase * impact.magnitude * (impact.fadeTime + hattuvakio) / baseImpactFadeTime / waveLength;
						}
					}
				}
			}
			impact.fadeTime -= delta;
			if (impact.fadeTime < 0F) {
				impacts.RemoveAt(i);
			}
		}
		//for (int i = 0; i < heightMap.Length; i++) {
		//	verts[i].y = verts[i].y * (1F - 0.2F * delta) + heightMap[i] * 0.2F * delta;
		//}
		GetComponent<MeshFilter>().mesh.vertices = verts;
		GetComponent<MeshFilter>().mesh.RecalculateNormals();
	}
	
	public void AddImpact(Vector2 pos, float magnitude, float heightMultp) {
		Impact impact = new Impact();
		impact.pos = pos / gridSpacing;
		impact.time = Time.time;
		impact.magnitude = magnitude * 20F * heightMultp;
		impact.fadeTime = baseImpactFadeTime;
		impacts.Add(impact);
		CreateCrater(pos / gridSpacing, magnitude * 100F, heightMultp);
	}
	
	public float GetDistanceToGround(Vector3 pos) {
		Vector3 tempPos = pos / gridSpacing;
		int x1 = Mathf.FloorToInt(tempPos.x);
		int x2 = x1 + 1;
		int y1 = Mathf.FloorToInt(tempPos.z);
		int y2 = y1 + 1;
		if (x1 < 0 || x2 > xSize || y1 < 0 || y2 > ySize) {
			return -1F;
		}
		//if (x1 < 0) {
		//	x1 = 0;
		//}
		//else if (x2 > xSize) {
		//	x2 = xSize;
		//}
		//if (y1 < 0) {
		//	y1 = 0;
		//}
		//else if (y2 > ySize) {
		//	y2 = ySize;
		//}
		float x1Weight = 1F - (tempPos.x - x1);
		float x2Weight = 1F - (x2 - tempPos.x);
		float y1Weight = 1F - (tempPos.z - y1);
		float y2Weight = 1F - (y2 - tempPos.z);
		float weightx1y1 = x1Weight * y1Weight;
		float weightx1y2 = x1Weight * y2Weight;
		float weightx2y1 = x2Weight * y1Weight;
		float weightx2y2 = x2Weight * y2Weight;
		float yDist = 	(verts[y1 * (xSize + 1) + x1].y * weightx1y1 + 
						verts[y2 * (xSize + 1) + x1].y * weightx1y2 +
						verts[y1 * (xSize + 1) + x2].y * weightx2y1 +
						verts[y2 * (xSize + 1) + x2].y * weightx2y2);
		return pos.y - yDist - offset;
	}
	
	public float GetGroundHeight(Vector3 pos) {
		Vector3 tempPos = pos * 2F;
		int x1 = Mathf.FloorToInt(tempPos.x);
		int x2 = x1 + 1;
		int y1 = Mathf.FloorToInt(tempPos.z);
		int y2 = y1 + 1;
		if (x1 < 0 || x2 > xSize || y1 < 0 || y2 > ySize) {
			return -1F;
		}
		float x1Weight = 1F - (tempPos.x - x1);
		float x2Weight = 1F - (x2 - tempPos.x);
		float y1Weight = 1F - (tempPos.z - y1);
		float y2Weight = 1F - (y2 - tempPos.z);
		float weightx1y1 = x1Weight * y1Weight;
		float weightx1y2 = x1Weight * y2Weight;
		float weightx2y1 = x2Weight * y1Weight;
		float weightx2y2 = x2Weight * y2Weight;
		float height = 	(verts[y1 * (xSize + 1) + x1].y * weightx1y1 + 
						verts[y2 * (xSize + 1) + x1].y * weightx1y2 +
						verts[y1 * (xSize + 1) + x2].y * weightx2y1 +
						verts[y2 * (xSize + 1) + x2].y * weightx2y2);
		return height + offset;
	}

	public Vector3 GetNormal(Vector3 pos) {
		Vector3 tempPos = pos * 2F;
		int x1 = Mathf.FloorToInt(tempPos.x);
		int x2 = x1 + 1;
		int z1 = Mathf.FloorToInt(tempPos.z);
		int z2 = z1 + 1;
		if (x1 < 0 || x2 > xSize || z1 < 0 || z2 > ySize) {
			return Vector3.up;
		}
		Vector3 v1 = new Vector3(x1 / 2f, verts[z1 * (xSize + 1) + x1].y - offset, z1 / 2f);
		Vector3 v2 = new Vector3(x1 / 2f, verts[z2 * (xSize + 1) + x1].y - offset, z2 / 2f);
		Vector3 v3 = new Vector3(x2 / 2f, verts[z1 * (xSize + 1) + x2].y - offset, z1 / 2f);
		Vector3 v4 = new Vector3(x2 / 2f, verts[z2 * (xSize + 1) + x2].y - offset, z2 / 2f);
		//print("v1; " + v1);
		//print("v2; " + v2);
		//print("v3; " + v3);
		//print("v4; " + v4);

		Vector3 vect1 = Vector3.Cross(v2 - v1, v3 - v1);
		//Vector3 vect2 = Vector3.Cross(v1 - v2, v4 - v2) * -1f;
		Vector3 vect3 = Vector3.Cross(v1 - v3, v4 - v3);
		//Vector3 vect4 = Vector3.Cross(v2 - v4, v3 - v4) * -1f;
		//print("1: " + vect1);
		//print("2: " + vect2);
		//print("3: " + vect3);
		//print("4: " + vect4);

		//return (vect1 + vect2 + vect3 + vect4).normalized;
		return (vect1 + vect3).normalized;
	}
	
	public void CreateCrater(Vector2 pos, float distance, float rimDistance) {
		int xHit = Mathf.RoundToInt(pos.x);
		int yHit = Mathf.RoundToInt(pos.y);
		int intDistance = Mathf.CeilToInt(distance);
		float holeAreaMultp = (1f - holeShape.Evaluate(rimDistance)) / rimDistance;
		float strength = 2F;
		float curveValue = 0f;
		Vector2 temp = Vector2.zero;
		int hmIndex = 0;
		for (int y = yHit - intDistance; y < yHit + intDistance + 1; y++) {
			for (int x = xHit - intDistance; x < xHit + intDistance + 1; x++) {
				if (x < 0 || x > xSize || y < 0 || y > ySize) {
					continue;
				}
				temp.x = x;
				temp.y = y;
				float dist = new Vector2(pos.x - temp.x, pos.y - temp.y).magnitude;
				if (dist > distance) {
					continue;
				}
				float centerDistance = dist / distance;
				if (centerDistance < holeAreaMultp) {
					curveValue = holeShape.Evaluate(centerDistance / holeAreaMultp) * strength * rimDistance;
					hmIndex = y * (xSize + 1) + x;
					heightMap[hmIndex] -= curveValue;
					if (heightMap[hmIndex] < 0F) {
						verts[hmIndex].y -= curveValue + heightMap[hmIndex];
						heightMap[hmIndex] = 0F;
					}
					else {
						verts[hmIndex].y -= curveValue;
					}
				}
			}
		}
		CreateCraterRock(pos, (distance * holeAreaMultp) / 3F);
		alphaTex.SetPixels(colors);
		alphaTex.Apply(false);
	}
	
	void CreateCraterRock(Vector2 pos, float distance) {
		int xHit = Mathf.RoundToInt(pos.x);
		int yHit = Mathf.RoundToInt(pos.y);
		int intDistance = Mathf.CeilToInt(distance);
		Vector2 temp = Vector2.zero;
		for (int y = yHit - intDistance; y < yHit + intDistance + 1; y++) {
			for (int x = xHit - intDistance; x < xHit + intDistance + 1; x++) {
				if (x < 0 || x > xSize - 1 || y < 0 || y > ySize - 1) {
					continue;
				}
				temp.x = x;
				temp.y = y;
				float dist = new Vector2(pos.x - temp.x, pos.y - temp.y).magnitude;
				if (dist > distance) {
					continue;
				}
				colors[(ySize - y - 1) * xSize + x] = new Color(0F, 0F, 0F, 1F);
			}
		}
	}
	
	void InitializeAlphaMap() {
		// alphaTex = new Texture2D(xSize, ySize);
		colors = new Color[xSize * ySize];
		for(int i = 0; i < colors.Length; i++ ) {
			colors[i] = Color.white;
		}
		alphaTex.SetPixels(colors);
		alphaTex.Apply(false);
		alphaTex.wrapMode = TextureWrapMode.Clamp;
		Renderer r = GetComponent<Renderer>();
		r.material.SetTexture("_AlphaTex", alphaTex);
	}
	
	void InitializeHeightMap() {
		heightMap = new float[(xSize + 1) * (ySize + 1)];
		float xOrg = Random.Range(1F, 100F);
		float yOrg = Random.Range(1F, 100F);
		float scale = 1.5F;
		float baseHeight = -offset;
		float hillAmplitude = 11F;

		if (hills == true) {
			for (int y = 0; y < ySize + 1; y++) {
				for (int x = 0; x < xSize + 1; x++) {
					float xCoord = xOrg + (float)x / xSize * scale * 1.5f;
					float yCoord = yOrg + (float)y / ySize * scale * 1.5f;
					float sample = Mathf.PerlinNoise(xCoord, yCoord);
					heightMap[y * (xSize + 1) + x] = baseHeight + sample * hillAmplitude;
				}
			}
		}
		else {
			for (int i = 0; i < heightMap.Length; i++) {
				heightMap[i] = baseHeight;
			}
		}
	}
	
	void InitializeMesh() {
		MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
		meshFilter.mesh = new Mesh();
		meshFilter.mesh.name = "GroundMesh";
		verts = new Vector3[(xSize + 1) * (ySize + 1)];
		uvs = new Vector2[(xSize + 1) * (ySize + 1)];
		tris = new int[xSize * ySize * 6];

		//verts
		for (int y = 0;y < ySize + 1; y++) { 
			for (int x = 0; x < xSize + 1; x++) {
				verts[y * (xSize + 1) + x] = new Vector3(gridSpacing * x, heightMap[y * (xSize + 1) + x], gridSpacing * y);
			}
		}
		// UVs
			for (int y = 0;y < ySize + 1; y++) { 
				for (int x = 0; x < xSize + 1; x++) {
					uvs[y * (xSize + 1) + x] = new Vector2((float)x / (float)xSize, 1F - y / (float)ySize);
				}
			}
		//tris
		int counter = 0;
		for (int y = 0; y < ySize; y++) {
			for (int x = 0; x < xSize; x++) {
				tris[counter] = y * (xSize + 1) + x;
				tris[counter + 1] = y * (xSize + 1) + x + xSize + 1;
				tris[counter + 2] = y * (xSize + 1) + x + 1;
				tris[counter + 3] = y * (xSize + 1) + x + xSize + 1;
				tris[counter + 4] = y * (xSize + 1) + x + xSize + 2;
				tris[counter + 5] = y * (xSize + 1) + x + 1;
				counter += 6;
			}
		}
		meshFilter.mesh.vertices = verts;
		meshFilter.mesh.uv = uvs;
		meshFilter.mesh.triangles = tris;
		meshFilter.mesh.RecalculateNormals();
		Renderer renderer = GetComponent<Renderer>();
		renderer.material.mainTexture = tex;
		renderer.material.mainTexture.wrapMode = TextureWrapMode.Clamp;
	}

	public void InitializeSpawnPositions() {
		float backPadding = 5f;
		float sidePadding = 10f;
		if (_randomizeSpawnPositions == false) {
			playerSpawnPositions = new SpawnPosition[] {
				new SpawnPosition(new Vector3(xSize / 4f, 0f, backPadding), 0f),
				new SpawnPosition(new Vector3(xSize / 4f, 0f, ySize / 2f - backPadding), 180f),
				new SpawnPosition(new Vector3(backPadding, 0f, ySize / 4f), 90f),
				new SpawnPosition(new Vector3(xSize / 2f - backPadding, 0f, ySize / 4f), 270f)
			};
		}
		else {
			playerSpawnPositions = new SpawnPosition[] {
				new SpawnPosition(new Vector3(Random.Range(sidePadding, xSize / 2f - sidePadding), 0f, backPadding), 0f),
				new SpawnPosition(new Vector3(backPadding, 0f, Random.Range(sidePadding, ySize / 2f - sidePadding)), 90f),
				new SpawnPosition(new Vector3(Random.Range(sidePadding, xSize / 2f - sidePadding), 0f, ySize / 2f - backPadding), 180f),
				new SpawnPosition(new Vector3(xSize / 2f - backPadding, 0f, Random.Range(sidePadding, ySize / 2f - sidePadding)), 270f)
			};
			GameManager.ShuffleArray(ref playerSpawnPositions);
		}
	}
}
