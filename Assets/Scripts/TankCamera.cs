﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TankCamera : MonoBehaviour {

	public enum ShakeMode { Constant, Smooth, VerySmooth }
	[HideInInspector]
	public Camera _camera;

	private bool _isLocalPlayer;
	private Vector2 _currentRot;
	private Vector2 _sensitivity = new Vector2(160f, 80f);
	private float _sensitivityMultp = 1f;
	private Tank _player;
	private string _controlString;
	private float _defaultFow;

	private float _distanceFromPlayer = 6f;
	private float _cameraYRotOffset = 40f;
	private Vector3 _playerLookOffset = new Vector3(0f, 1f, 0f);
	private float _groundPadding = 0.5f;
	private float _wallPadding = 0.2f;

	private Vector2 _levelSize;
	
	// Camera shake
	private ShakeMode shakeMode = ShakeMode.Smooth;
	private float shakeStrength = 0.2F;
	private bool cameraShake = false;
	private float normalizedShakeTime = 0F;
	private float shakeTime = 0F;
	private float timeFactor = 1F;

	public void Initialize(Transform player, bool isLocalPlayer, float facing, float sensitivity, LayerMask mask) {
		if (GameManager.instance._gameMode == GameManager.GameMode.Network) {
			_isLocalPlayer = isLocalPlayer;
			if (_isLocalPlayer == false) {
				gameObject.SetActive(false);
				return;
			}
		}
		_levelSize = CreateLevel.instance._worldSize;
		_player = player.GetComponent<Tank>();
		//if (_player._playerIndex == 1) {
		//	_sensitivity = new Vector2(_sensitivity.x, _sensitivity.y);
		//}
		_controlString = _player.GetComponent<Tank>()._controlString;
		_camera = GetComponent<Camera>();
		_camera.cullingMask = mask;

		_defaultFow = _camera.fieldOfView;
		_currentRot = new Vector2(facing, -20f);
		ChangeSensitivity(sensitivity);

		// Copy-pasta from LateUpdate
		Vector3 direction = new Vector3(0f, 0f, -_distanceFromPlayer);
		Quaternion rotation = Quaternion.Euler(_currentRot.y + _cameraYRotOffset, _currentRot.x, 0f);
		transform.position = _player.transform.position + rotation * direction;
		transform.LookAt(_player.transform.position + _playerLookOffset);
	}

	public void ChangeSensitivity(float newSensitivity) {
		_sensitivityMultp = newSensitivity;
	}

	private void Update() {
		// Camera currently moves during score screen, but is not visible
		if (GameManager.instance._gameMode == GameManager.GameMode.Local || _isLocalPlayer == true) {
			//print("Player" + _player._playerIndex + ": Camera update");

			if (GameManager.instance.gameOver == false) {
				_currentRot = new Vector2(_currentRot.x + Input.GetAxis(_controlString + "AimX") * _sensitivity.x * _sensitivityMultp * Time.deltaTime,
							Mathf.Clamp(_currentRot.y - Input.GetAxis(_controlString + "AimY") * _sensitivity.y * _sensitivityMultp * Time.deltaTime, -35f, 10f));
			}
		}
	}

	private void LateUpdate() {
		if (GameManager.instance._gameMode == GameManager.GameMode.Local || _isLocalPlayer == true) {
			Vector3 direction = new Vector3(0f, 0f, -_distanceFromPlayer);
			Quaternion rotation = Quaternion.Euler(_currentRot.y + _cameraYRotOffset, _currentRot.x, 0f);
			transform.position = _player.transform.position + rotation * direction;
			if (transform.position.x > 0f && transform.position.x < 64f && transform.position.z > 0f && transform.position.z < 64f) {
				if (CreateLevel.instance.GetDistanceToGround(transform.position) < _groundPadding) {
					float height = CreateLevel.instance.GetGroundHeight(transform.position);
					transform.position = new Vector3(transform.position.x, height + _groundPadding, transform.position.z);
				}
			}
			//// Clamp transform to level bounds
			//if (transform.position.x - _wallPadding < 0f) {
			//	transform.position = new Vector3(_wallPadding, transform.position.y, transform.position.z);
			//}
			//else if (transform.position.x + _wallPadding > _levelSize.x) {
			//	transform.position = new Vector3(_levelSize.x - _wallPadding, transform.position.y, transform.position.z);
			//}
			//if (transform.position.z - _wallPadding < 0f) {
			//	transform.position = new Vector3(transform.position.x, transform.position.y, _wallPadding);
			//}
			//else if (transform.position.z + _wallPadding > _levelSize.y) {
			//	transform.position = new Vector3(transform.position.x, transform.position.y, _levelSize.y - _wallPadding);
			//}

			//if (CreateLevel.instance.GetDistanceToGround(transform.position) < _groundPadding) {
			//	float height = CreateLevel.instance.GetGroundHeight(transform.position);
			//	transform.position = new Vector3(transform.position.x, height + _groundPadding, transform.position.z);
			//}
			transform.LookAt(_player.transform.position + _playerLookOffset);

			// Camera shake
			if (cameraShake == true && Time.timeScale != 0) {
				shakeTime -= Time.deltaTime;
				if (shakeTime <= 0f) {
					cameraShake = false;
				}
				normalizedShakeTime += Time.deltaTime * timeFactor;
				float shakeMagnitude;
				Vector3 shakePosition = new Vector3(0, 0, 0);
				if (shakeMode == ShakeMode.Smooth) {
					shakeMagnitude = (1 - ((2 * normalizedShakeTime - 1) * (2 * normalizedShakeTime - 1))) * shakeStrength;
					shakePosition.x += Random.Range(0F, 1F * shakeMagnitude);
					shakePosition.y += Random.Range(0F, 1F * shakeMagnitude);
				}
				else if (shakeMode == ShakeMode.VerySmooth) {
					shakeMagnitude = (Mathf.Pow(2 * normalizedShakeTime - 1, 4) - 2 * Mathf.Pow(2 * normalizedShakeTime - 1, 2) + 1) * shakeStrength;
					shakePosition.x += Random.Range(0F, 1F * shakeMagnitude);
					shakePosition.y += Random.Range(0F, 1F * shakeMagnitude);
				}
				else if (shakeMode == ShakeMode.Constant) {
					shakePosition.x += Random.Range(0F, 1F * shakeStrength);
					shakePosition.y += Random.Range(0F, 1F * shakeStrength);
				}
				transform.position += shakePosition;
			}
		}
	}

	public void CameraShake(float duration, float strength = 1F, ShakeMode mode = ShakeMode.VerySmooth) {
		cameraShake = true;
		shakeStrength = strength;
		if (shakeTime < duration) {
			shakeTime = duration;
		}
		shakeMode = mode;
		timeFactor = 1F / shakeTime;
		normalizedShakeTime = 0F;
	}

	public void AdjustFow(float multp) {
		if (GameManager.instance.cinematic == false) {
			_camera.fieldOfView = _defaultFow * multp;
		}
	}
}
