﻿using UnityEngine;
//using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class CannonBall : NetworkBehaviour {

	public static float _sizePerDamage = 8f / 60f;
	public static float _wavePerDamage = 0.2f / 60f;

	public float _explosionSize = 2f;
	[SyncVar]
	public Vector3 _motionVector;
	public AnimationCurve _damageFalloff;
	public AnimationCurve _screenShakeFalloff;

	private float _maxDamage = 60f;
	private Transform _owner;
	private float _ownerGraceTime = 0.5f;
	private float _graceTimer = 0f;
	private List<Transform> _hitPlayers = new List<Transform>();
	private MeshRenderer _renderer;
	private Rigidbody _rb;
	[SyncVar]
	private bool _active = false;

	private float[] _sizeByTankType = new float[] { 0.4f, 0.5f, 0.6f };

	private void Awake() {
		_renderer = GetComponent<MeshRenderer>();
		_rb = GetComponent<Rigidbody>();
		Disable();
	}

	public void Disable() {
		_active = false;
		_rb.detectCollisions = false;
		_rb.useGravity = false;
		_rb.velocity = Vector3.zero;
		if (GameManager.instance._gameMode == GameManager.GameMode.Local) {
			_renderer.enabled = false;
		}
		else {
			CmdSetRenderer(false);
		}
	}

	public void Enable(Transform owner, Vector3 startPos, Vector3 motionVector, float damage, Tank.Type tankType) {
		_active = true;
		transform.position = startPos;
		transform.localScale = Vector3.one * _sizeByTankType[(int)tankType];
		_motionVector = motionVector;
		_owner = owner;
		_graceTimer = _ownerGraceTime;
		_rb.detectCollisions = true;
		_rb.useGravity = true;
		_maxDamage = damage;
		_explosionSize = _sizePerDamage * damage;
		if (GameManager.instance._gameMode == GameManager.GameMode.Local) {
			_renderer.enabled = true;
		}
		else {
			CmdSetRenderer(true);
		}
	}

	private void Update() {
		if (GameManager.instance._gameMode == GameManager.GameMode.Local || isServer == true) {
			if (_active == true) {
				_graceTimer -= Time.deltaTime;
				
				if (transform.position.x > 0f && transform.position.x < CreateLevel.instance._worldSize.x && transform.position.z > 0f && transform.position.z < CreateLevel.instance._worldSize.y) {
					if (CreateLevel.instance.GetDistanceToGround(transform.position) <= 0f) {
						Explode();
					}
				}
				if (transform.position.y < -10f) {
					GameManager.instance.UnSpawnBall(gameObject);
				}
			}
		}
	}

	private void FixedUpdate() {
		if (_active == true) {
			_motionVector -= Vector3.up * Tank._G * Time.deltaTime;
			transform.Translate(_motionVector * Time.deltaTime);
		}
	}

	private void OnTriggerEnter(Collider collider) {
		if (GameManager.instance._gameMode == GameManager.GameMode.Local || isServer == true) {
			if (collider.tag == "Player") {
				if (collider.transform == _owner) {
					if (_graceTimer < 0f) {
						Explode(collider.transform);
					}
				}
				else {
					Explode(collider.transform);
				}
			}
			else {
				Vector3 overVector = new Vector3();
				// Clamp ball inside level, so waves can happen
				if (transform.position.x < 0f) {
					overVector.x += transform.position.x;
				}
				else if (transform.position.x > CreateLevel.instance._worldSize.x) {
					overVector.x += transform.position.x - (CreateLevel.instance._worldSize.x);
				}
				if (transform.position.z < 0f) {
					overVector.z += transform.position.z;
				}
				else if (transform.position.z > CreateLevel.instance._worldSize.y) {
					overVector.z += transform.position.z - (CreateLevel.instance._worldSize.y);
				}
				transform.position -= overVector * 1.01f; // 1.01 multiplier so that position is under level size
				Explode();
			}
		}
	}

	private void Explode(Transform directHitPlayer = null) {
		for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
			if (GameManager._playerWrappers[i] != null) {
				GameManager._playerWrappers[i]._player._cameraScript.CameraShake(1f,
					_screenShakeFalloff.Evaluate(Vector3.Distance(GameManager._playerWrappers[i]._player.transform.position, transform.position) / _explosionSize));
			}
		}
		float damageDealt = 0f;
		float selfDamage = 0f;
		int kills = 0;
		
		// Check for direct hit
		if (directHitPlayer != null) {
			float damageDone = directHitPlayer.GetComponent<Tank>().TakeDamage(_maxDamage);
			if (directHitPlayer.GetComponent<Tank>()._health <= 0f && damageDone > 0f) {
				kills++;
			}
			_hitPlayers.Add(directHitPlayer);
			if (directHitPlayer != _owner) {
				damageDealt += damageDone;
			}
			else {
				selfDamage += damageDone;
			}
		}
		// Check for area hit
		Collider[] aoeHits = Physics.OverlapSphere(transform.position, _explosionSize / 2f, GameManager.instance._playerMask);
		for (int i = 0; i < aoeHits.Length; i++) {
			if (_hitPlayers.Count == 0 || _hitPlayers.Contains(aoeHits[i].transform) == false) {
				float distance = (aoeHits[i].transform.position - transform.position).magnitude;
				float damage = _maxDamage * _damageFalloff.Evaluate(distance / _explosionSize * 2f);
				float damageDone = aoeHits[i].transform.GetComponent<Tank>().TakeDamage(damage);
				if (aoeHits[i].transform.GetComponent<Tank>()._health <= 0f && damageDone > 0f && aoeHits[i].transform != _owner) {
					kills++;
				}
				if (aoeHits[i].transform != _owner) {
					damageDealt += damageDone;
				}
				else {
					selfDamage += damageDone;
				}
			}
		}

		if (damageDealt > 0f) {
			_owner.GetComponent<Tank>().ShotHit(damageDealt, kills);
		}
		if (selfDamage > 0f) {
			_owner.GetComponent<Tank>()._selfDamage += selfDamage;
			if (_owner.GetComponent<Tank>()._health <= 0f) {
				_owner.GetComponent<Tank>()._suicide++;
			}
		}

		GameManager.instance.UnSpawnBall(gameObject);
		if (GameManager.instance._gameMode == GameManager.GameMode.Network) {
			RpcSetExplosion(transform.position);
		}
		else {
			SetExplosion(transform.position);
		}
	}

	private void SetExplosion(Vector3 position) {
		//print("explosion, yay " + isServer);
		float height = CreateLevel.instance.GetDistanceToGround(position);
		//print(height);
		if (height < _explosionSize / 2f) {
			if (height < 0f) {
				height = 0f;
			}
			float heightMultp = (1f - Mathf.Clamp01(height / (_explosionSize / 2f)));
			CreateLevel.instance.AddImpact(new Vector2(transform.position.x, transform.position.z), _wavePerDamage * _maxDamage, heightMultp);
		}
		GameObject explosion = GameManager.instance.GetExplosion();
		explosion.GetComponent<ExplosionSphere>().Enable(position, _explosionSize);

		float shortestDistance = 9999f;
		for (int i = 0; i < GameManager._playerWrappers.Length; i++) {
			if (GameManager._playerWrappers[i] != null) {
				if (Vector3.Distance(transform.position, GameManager._playerWrappers[i]._player.transform.position) < shortestDistance) {
					shortestDistance = Vector3.Distance(transform.position, GameManager._playerWrappers[i]._player.transform.position);
				}
			}
		}
		GameManager.instance.PlaySound(GameManager.instance._explosionSound, _screenShakeFalloff.Evaluate((shortestDistance / 2f) / _explosionSize) * 0.8f);
	}

	[ClientCallback]
	public void CmdSetRenderer(bool b) {
		_renderer.enabled = b;
	}

	[ClientRpc]
	public void RpcSetExplosion(Vector3 position) {
		NetworkServer.UnSpawn(gameObject);
		SetExplosion(position);
	}
}
