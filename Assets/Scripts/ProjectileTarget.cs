﻿using UnityEngine;
//using System.Collections;

public class ProjectileTarget : MonoBehaviour {

	private Transform _childSprite;
	private float _spriteRotateSpeed = 60f;

	private void Awake() {
		_childSprite = transform.GetChild(0);
	}

	private void Update() {
		float frontHeight = CreateLevel.instance.GetGroundHeight(transform.position + transform.forward * 0.75F);
		float backHeight = CreateLevel.instance.GetGroundHeight(transform.position - transform.forward * 0.75F);
		float rightHeight = CreateLevel.instance.GetGroundHeight(transform.position + transform.right * 0.5F);
		float leftHeight = CreateLevel.instance.GetGroundHeight(transform.position - transform.right * 0.5F);
		float pitchAngle = -Mathf.Atan((frontHeight - backHeight) / 1.5F) * Mathf.Rad2Deg;
		float rollAngle = Mathf.Atan((rightHeight - leftHeight) / 1.5F) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(pitchAngle, transform.rotation.eulerAngles.y, rollAngle);

		_childSprite.Rotate(Vector3.forward, _spriteRotateSpeed * Time.deltaTime);
		//_childSprite.Rotate(Vector3.up, _childSprite.rotation.eulerAngles.y + _spriteRotateSpeed * Time.deltaTime);
	}
}
