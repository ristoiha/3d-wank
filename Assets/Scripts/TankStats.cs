﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankStats : MonoBehaviour {
	
	public class TStats {
		public float damage = 60F;
		public float speed = 7F;
		public float hitPoints = 100F;
		public float turbo = 100F;
		public float shotCooldown = 0.75F;
		public float turboCooldown = 1.5f;
	}
	
	public enum StatType { Damage, Speed, HitPoints, Turbo, ShotCooldown, TurboCooldown };
	public static TStats[] tankStats = new TStats[Tank.Type.GetNames(typeof(Tank.Type)).Length];
	
	private static float minDamage = 1000F;
	private static float maxDamage = 0F;
	private static float minSpeed = 1000F;
	private static float maxSpeed = 0F;
	private static float minHitPoints = 1000F;
	private static float maxHitPoints = 0F;
	private static float minTurbo = 1000F;
	private static float maxTurbo = 0F;
	
	void Awake() {
		for (int i = 0; i < tankStats.Length; i++) {
			tankStats[i] = new TStats();
		}
		
		tankStats[(int)Tank.Type.Light].damage = 40F;
		tankStats[(int)Tank.Type.Light].speed = 10F;
		tankStats[(int)Tank.Type.Light].hitPoints = 80F;
		tankStats[(int)Tank.Type.Light].turbo = 120F;
		tankStats[(int)Tank.Type.Light].shotCooldown = 0.6f;
		tankStats[(int)Tank.Type.Light].turboCooldown = 1f;

		tankStats[(int)Tank.Type.Medium].damage = 60F;
		tankStats[(int)Tank.Type.Medium].speed = 7F;
		tankStats[(int)Tank.Type.Medium].hitPoints = 100F;
		tankStats[(int)Tank.Type.Medium].turbo = 100F;
		tankStats[(int)Tank.Type.Medium].shotCooldown = 0.75f;
		tankStats[(int)Tank.Type.Medium].turboCooldown = 1.5f;

		tankStats[(int)Tank.Type.Heavy].damage = 80F;
		tankStats[(int)Tank.Type.Heavy].speed = 5F;
		tankStats[(int)Tank.Type.Heavy].hitPoints = 150F;
		tankStats[(int)Tank.Type.Heavy].turbo = 60F;
		tankStats[(int)Tank.Type.Heavy].shotCooldown = 0.9f;
		tankStats[(int)Tank.Type.Heavy].turboCooldown = 1.5f;

		CalculateStatBounds();
	}
	
	private void CalculateStatBounds() {
		for (int i = 0; i < tankStats.Length; i++) {
			if (minDamage > tankStats[i].damage) {
				minDamage = tankStats[i].damage;
			}
			if (maxDamage < tankStats[i].damage) {
				maxDamage = tankStats[i].damage;
			}
			if (minSpeed > tankStats[i].speed) {
				minSpeed = tankStats[i].speed;
			}
			if (maxSpeed < tankStats[i].speed) {
				maxSpeed = tankStats[i].speed;
			}
			if (minHitPoints > tankStats[i].hitPoints) {
				minHitPoints = tankStats[i].hitPoints;
			}
			if (maxHitPoints < tankStats[i].hitPoints) {
				maxHitPoints = tankStats[i].hitPoints;
			}
			if (minTurbo > tankStats[i].turbo) {
				minTurbo = tankStats[i].turbo;
			}
			if (maxTurbo < tankStats[i].turbo) {
				maxTurbo = tankStats[i].turbo;
			}
		}
	}
	
	public static float GetStat(Tank.Type type, TankStats.StatType stat) {
		if (stat == TankStats.StatType.Damage) {
			return tankStats[(int)type].damage;
		}
		else if (stat == TankStats.StatType.Speed) {
			return tankStats[(int)type].speed;
		}
		else if (stat == TankStats.StatType.HitPoints) {
			return tankStats[(int)type].hitPoints;
		}
		else if (stat == TankStats.StatType.Turbo) {
			return tankStats[(int)type].turbo;
		}
		else if (stat == TankStats.StatType.ShotCooldown) {
			return tankStats[(int)type].shotCooldown;
		}
		else {
			return tankStats[(int)type].turboCooldown;
		}
	}
	
	public static float GetMinStat(TankStats.StatType stat) {
		if (stat == TankStats.StatType.Damage) {
			return minDamage;
		}
		else if (stat == TankStats.StatType.Speed) {
			return minSpeed;
		}
		else if (stat == TankStats.StatType.HitPoints) {
			return minHitPoints;
		}
		else {
			return minTurbo;
		}
	}
	
	public static float GetMaxStat(TankStats.StatType stat) {
		if (stat == TankStats.StatType.Damage) {
			return maxDamage;
		}
		else if (stat == TankStats.StatType.Speed) {
			return maxSpeed;
		}
		else if (stat == TankStats.StatType.HitPoints) {
			return maxHitPoints;
		}
		else {
			return maxTurbo;
		}
	}
}
