﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.EventSystems;

public class GameManager : NetworkManager {

	public class PlayerWrapper {
		public int _controllerIndex;
		public Tank _player;

		//public PlayerWrapper(int controllerIndex, Tank player) {
		//	_controllerIndex = controllerIndex;
		//	_player = player;
		//}

		public PlayerWrapper(int controllerIndex) {
			_controllerIndex = controllerIndex;
		}
	}

	public enum GameMode { Local, Network }

	public static GameManager instance;
	public static int _maxPlayers = 4;
	public static int _maxControllers = 5;
	public static PlayerWrapper[] _playerWrappers = new PlayerWrapper[_maxPlayers];
	public static int _alivePlayers;
	public static float _joystickDeadZone = 0.3f;

	public GameMode _gameMode = GameMode.Local;
	public GameObject[] tankPrefabs;
	public int _gameRound = 0;
	//public int _playersInGame = 2;
	public LayerMask _playerMask;
	public GameObject _ballPrefab;
	public GameObject _explosionPrefab;
	public Color[] _playerColors;
	public LayerMask[] _renderMasks;
	public Gradient _healthGradient;
	public AnimationCurve _heatOverlayCurve;
	public AnimationCurve _heatCannonCurve;
	public AnimationCurve _heatAlertCurve;

	[Header("Camera drive")]
	public AnimationCurve _cameraHeightCurve;
	public AnimationCurve _cameraDistanceCurve;
	public AnimationCurve _cameraRotationCurve;
	private float _cameraStartHeight = 20f;
	private float _cameraStartDistance = 32f;

	[Header("Menus")]
	public Transform _mainMenu;
	public Transform _pauseMenu;

	[Header("Audio")]
	public AudioClip _shootSound;
	public AudioClip _explosionSound;
	public AudioClip _playerHitSound;
	public AudioClip _playerDeathSound;
	public AudioClip _groundImpactSound;
	public AudioClip _overheatSound;

	[HideInInspector]
	public bool inGame = false;
	[HideInInspector]
	public bool cinematic = false;
	[HideInInspector]
	public bool gameOver = false;
	[HideInInspector]
	public bool paused = false;

	private bool _cursorLocked = false;
	
	public AudioSource _gameAudioSource;
	public AudioSource _musicAudioSource;
	public AudioSource _menuAudioSource;

	private int _poolSize = 20;
	private GameObject[] _ballPool;
	private GameObject[] _explosionPool;
	private int _ballIndex;
	private int _explosionIndex;
	public ServerParameters sParams;
	public delegate GameObject SpawnDelegate(Vector3 position, NetworkHash128 assetId);
	public delegate void UnSpawnDelegate(GameObject spawned);
	[HideInInspector]
	public NetworkHash128 _ballAssetId;

	private List<Transform> _currentTanks = new List<Transform>();
	
	public override void OnStartHost() {
		base.OnStartHost();
		sParams.randomSeed = Random.Range(0, 65536);
	}
	
	public void OnStartClient() {
		NetworkServer.SpawnObjects();
	}

	private void Start() {
		instance = this;

		// Select local game button
		_mainMenu.gameObject.SetActive(true);
		_mainMenu.GetChild(1).GetChild(0).GetComponent<UnityEngine.UI.Selectable>().Select();

		for (int i = 0; i < _playerWrappers.Length; i++) {
			_playerWrappers[i] = null;
		}

		_ballAssetId = _ballPrefab.GetComponent<NetworkIdentity>().assetId;
		_ballPool = new GameObject[_poolSize];
		_explosionPool = new GameObject[_poolSize];
		for (int i = 0; i < _poolSize; i++) {
			GameObject ball = Instantiate(_ballPrefab, transform.position, Quaternion.identity, transform.GetChild(0));
			_ballPool[i] = ball;
			GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity, transform.GetChild(1));
			_explosionPool[i] = explosion;
		}
		ClientScene.RegisterSpawnHandler(_ballAssetId, SpawnBall, UnSpawnBall);

		if (_gameMode == GameMode.Local) {
			sParams.randomSeed = Random.Range(0, 65536);
			// Tests trying to fix local crash
			//Shutdown();
			//StopServer();
			GetComponent<NetworkManagerHUD>().showGUI = false;
			CreateLevel.instance.GenerateLevel();
			//StartLocalRound();
			//StartCoroutine(LocalGameRoutine());
		}
	}

	//private IEnumerator LocalGameRoutine() {
	//	yield return new WaitForSeconds(1f);
	//	sParams.randomSeed = Random.Range(0, 65536);
	//	CreateLevel.instance.GenerateLevel();
	//	StartLocalGame();
	//}

	private void Update() {
		if (inGame == true) {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				I_PauseGame();
			}
			for (int i = 0; i < _playerWrappers.Length; i++) {
				if (_playerWrappers[i] != null) {
					if (_playerWrappers[i]._controllerIndex != 0) {
						if (Input.GetButtonDown(_playerWrappers[i]._player._controlString + "Menu")) {
							I_PauseGame();
						}
					}
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.E)) {
			_cursorLocked = !_cursorLocked;
		}
		//if (Input.GetKeyDown(KeyCode.Q)) {
		//	I_StartLocalGame();
		//}
		if (_cursorLocked == true) {
			Cursor.lockState = CursorLockMode.Locked;
		}
		else {
			Cursor.lockState = CursorLockMode.None;
		}
	}

	public GameObject GetBall() {
		GameObject ball = _ballPool[_ballIndex];
		_ballIndex++;
		if (_ballIndex == _ballPool.Length) {
			_ballIndex = 0;
		}
		return ball;
	}

	public GameObject GetExplosion() {
		GameObject explosion = _explosionPool[_explosionIndex];
		_explosionIndex++;
		if (_explosionIndex == _explosionPool.Length) {
			_explosionIndex = 0;
		}
		return explosion;
	}

	public void PlaySound(AudioClip clip, float volume) {
		_gameAudioSource.PlayOneShot(clip, volume);
	}

	public void I_StartLocalGame() {
		// Show Score UI, handle player joining...
		// Reset player wrappers
		_playerWrappers = new PlayerWrapper[_maxPlayers];
		for (int i = 0; i < _playerWrappers.Length; i++) {
			_playerWrappers[i] = null;
		}
		_gameRound = 0;
		_mainMenu.gameObject.SetActive(false);
		EventSystem.current.SetSelectedGameObject(null);
		StatScreenManager.instance.ShowGameStartScreen();
		//StartLocalRound();
	}

	public void I_StartOnlineGame() {
		// Empty for now - Open different menu etc
	}

	public void I_PauseGame() {
		if (paused == false) {
			paused = true;
			Time.timeScale = 0f;
			_pauseMenu.gameObject.SetActive(true);
			for (int i = 0; i < _playerWrappers.Length; i++) {
				if (_playerWrappers[i] != null) {
					_playerWrappers[i]._player.Pause(true);
				}
			}
			_gameAudioSource.Pause();
			_pauseMenu.GetChild(1).GetChild(1).GetChild(0).GetComponent<UnityEngine.UI.Selectable>().Select();
		}
		else {
			paused = false;
			Time.timeScale = 1f;
			_pauseMenu.gameObject.SetActive(false);
			for (int i = 0; i < _playerWrappers.Length; i++) {
				if (_playerWrappers[i] != null) {
					_playerWrappers[i]._player.Pause(false);
				}
			}
			_gameAudioSource.UnPause();
		}
	}

	public void I_ForceRoundEnd() {
		I_PauseGame();
		gameOver = true;
		inGame = false;
		cinematic = false;
		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				_playerWrappers[i]._player.GameOverTasks();
			}
		}
		CinematicCamera.instance.ShowCamera(true);
		StatScreenManager.instance.ShowMatchScreen();
	}

	public void I_BackToMainMenu() {
		I_PauseGame();
		gameOver = true;
		inGame = false;
		cinematic = false;
		for (int i = 0; i < _currentTanks.Count; i++) {
			Destroy(_currentTanks[i].gameObject);
		}
		_currentTanks.Clear();
		for (int i = 0; i < _playerWrappers.Length; i++) {
			_playerWrappers[i] = null;
		}
		_pauseMenu.gameObject.SetActive(false);
		_mainMenu.gameObject.SetActive(true);
		_mainMenu.GetChild(1).GetChild(0).GetComponent<UnityEngine.UI.Selectable>().Select();
		CinematicCamera.instance.ShowCamera(true);
	}

	public void I_QuitGame() {
		Application.Quit();
	}

	public void StartLocalRound() {
		sParams.randomSeed = Random.Range(0, 65536);
		CinematicCamera.instance.ShowCamera(false);
		CreateLevel.instance.GenerateLevel();
		_gameRound++;
		for (int i = 0; i < _currentTanks.Count; i++) {
			Destroy(_currentTanks[i].gameObject);
		}
		_currentTanks.Clear();
		_alivePlayers = 0;
		CreateLevel.instance.InitializeSpawnPositions();
		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				SpawnPlayer(i);
				_alivePlayers++;
			}
		}
		AdjustCameras();
		gameOver = false;
		inGame = true;
		cinematic = true;
		StartCoroutine(CameraDriveRoutine());
	}

	private IEnumerator CameraDriveRoutine() {
		float targetTime = 3f;
		Vector3[] cameraPosArray = new Vector3[_playerWrappers.Length];
		float[] cameraRotArray = new float[_playerWrappers.Length];
		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				_playerWrappers[i]._player._cameraScript.enabled = false;
				cameraPosArray[i] = _playerWrappers[i]._player._cameraScript.transform.localPosition;
				cameraRotArray[i] = _playerWrappers[i]._player._cameraScript.transform.localRotation.eulerAngles.x;
			}
		}

		for (float i = 0f; i <= targetTime; i += Time.deltaTime) {
			for (int j = 0; j < _playerWrappers.Length; j++) {
				if (_playerWrappers[j] != null) {
					_playerWrappers[j]._player._cameraScript.transform.localPosition = cameraPosArray[j] +
						(_playerWrappers[j]._player.transform.up * _cameraStartHeight * _cameraHeightCurve.Evaluate(i / targetTime)) +
						(_playerWrappers[j]._player._bottom.transform.forward * _cameraStartDistance * _cameraDistanceCurve.Evaluate(i / targetTime));
					_playerWrappers[j]._player._cameraScript.transform.localRotation = 
						Quaternion.Euler(90f - (90f - cameraRotArray[j]) * _cameraRotationCurve.Evaluate(i / targetTime), _playerWrappers[j]._player._cameraScript.transform.localRotation.eulerAngles.y, 0f);
					_playerWrappers[j]._player._cameraScript._camera.fieldOfView = 45f + 45f * _cameraHeightCurve.Evaluate(i / targetTime);
				}
			}
			yield return null;
		}

		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				_playerWrappers[i]._player._cameraScript.enabled = true;
				_playerWrappers[i]._player._uiCamera.enabled = true;
				_playerWrappers[i]._player._cameraScript.transform.localRotation =
					Quaternion.Euler(cameraRotArray[i], _playerWrappers[i]._player._cameraScript.transform.localRotation.eulerAngles.y, 0f);
			}
		}
		cinematic = false;
	}

	private GameObject SpawnPlayer(int playerIndex) {
		CreateLevel.SpawnPosition spawnPos = CreateLevel.instance.playerSpawnPositions[playerIndex];
		GameObject player;
		if ((Tank.Type)StatScreenManager.instance._playerSettings[playerIndex]._tankIndex == Tank.Type.Light) {
			player = Instantiate(tankPrefabs[0], spawnPos._position, Quaternion.identity) as GameObject;
		}
		else if ((Tank.Type)StatScreenManager.instance._playerSettings[playerIndex]._tankIndex == Tank.Type.Medium) {
			player = Instantiate(tankPrefabs[1], spawnPos._position, Quaternion.identity) as GameObject;
		}
		else {
			player = Instantiate(tankPrefabs[2], spawnPos._position, Quaternion.identity) as GameObject;
		}
		player.name = "Player" + playerIndex;
		Tank tankScript = player.GetComponent<Tank>();
		tankScript.Initialize(playerIndex, spawnPos._facing);
		_playerWrappers[playerIndex]._player = tankScript;
		_currentTanks.Add(player.transform);
		return player;
	}

	void AdjustCameras() {
		float horP = 0.002f;
		float verP = 0.003f;
		List<int> playerIndexList = new List<int>();
		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				playerIndexList.Add(i);
			}
		}

		if (playerIndexList.Count == 1) {
			_playerWrappers[playerIndexList[0]]._player._cameraScript._camera.rect = new Rect(0f, 0f, 1f, 1f);
			_playerWrappers[playerIndexList[0]]._player._uiCamera.rect = new Rect(0f, 0f, 1f, 1f);
		}
		else if (playerIndexList.Count == 2) {
			_playerWrappers[playerIndexList[0]]._player._cameraScript._camera.rect =	new Rect(0f, 0.5f + verP, 1f, 0.5f - verP);
			_playerWrappers[playerIndexList[0]]._player._uiCamera.rect =				new Rect(0f, 0.5f + verP, 1f, 0.5f - verP);
			_playerWrappers[playerIndexList[1]]._player._cameraScript._camera.rect =	new Rect(0f, 0f, 1f, 0.5f - verP);
			_playerWrappers[playerIndexList[1]]._player._uiCamera.rect =				new Rect(0f, 0f, 1f, 0.5f - verP);
		}
		else if (playerIndexList.Count == 3) {
			//_playerWrappers[playerIndexList[0]]._player._cameraScript._camera.rect =	new Rect(0f, 0.5f + verP, 1f, 0.5f - verP);
			//_playerWrappers[playerIndexList[0]]._player._uiCamera.rect =				new Rect(0f, 0.5f + verP, 1f, 0.5f - verP);

			_playerWrappers[playerIndexList[0]]._player._cameraScript._camera.rect =	new Rect(0.25f + horP /2f, 0.5f + verP, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[0]]._player._uiCamera.rect =				new Rect(0.25f + horP / 2f, 0.5f + verP, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[1]]._player._cameraScript._camera.rect =	new Rect(0f, 0f, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[1]]._player._uiCamera.rect =				new Rect(0f, 0f, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[2]]._player._cameraScript._camera.rect =	new Rect(0.5f + horP, 0f, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[2]]._player._uiCamera.rect =				new Rect(0.5f + horP, 0f, 0.5f - horP, 0.5f - verP);
		}
		else if (playerIndexList.Count == 4) {
			_playerWrappers[playerIndexList[0]]._player._cameraScript._camera.rect =	new Rect(0f, 0.5f + verP, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[0]]._player._uiCamera.rect =				new Rect(0f, 0.5f + verP, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[1]]._player._cameraScript._camera.rect =	new Rect(0.5f + horP, 0.5f + verP, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[1]]._player._uiCamera.rect =				new Rect(0.5f + horP, 0.5f + verP, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[2]]._player._cameraScript._camera.rect =	new Rect(0f, 0f, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[2]]._player._uiCamera.rect =				new Rect(0f, 0f, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[3]]._player._cameraScript._camera.rect =	new Rect(0.5f + horP, 0f, 0.5f - horP, 0.5f - verP);
			_playerWrappers[playerIndexList[3]]._player._uiCamera.rect =				new Rect(0.5f + horP, 0f, 0.5f - horP, 0.5f - verP);
		}
		// 0 players
		else {

		}
	}

	public void PlayerDeath() {
		if (gameOver == false) {
			_alivePlayers--;
			if (_alivePlayers == 1) {
				gameOver = true;
				StartCoroutine(GameOverRoutine());
			}
		}
	}

	IEnumerator GameOverRoutine() {
		int winner = -1;
		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null && _playerWrappers[i]._player._health > 0f) {
				winner = _playerWrappers[i]._player._playerIndex;
				_playerWrappers[i]._player.ShowHUDIcon(1);
			}
		}
		yield return new WaitForSeconds(1F);
		// Win / Lose
		if (_playerWrappers[winner]._player._health > 0f) {
			for (int i = 0; i < _playerWrappers.Length; i++) {
				if (_playerWrappers[i] != null) {
					if (_playerWrappers[i] != null && _playerWrappers[i]._player._playerIndex == winner) {
						_playerWrappers[i]._player.winnerText.gameObject.SetActive(true);
						_playerWrappers[i]._player._win++;
					}
					else {
						_playerWrappers[i]._player.loserText.gameObject.SetActive(true);
					}
				}
			}
		}
		// Draw
		else {
			for (int i = 0; i < _playerWrappers.Length; i++) {
				if (_playerWrappers[i] != null) {
					_playerWrappers[i]._player.drawText.gameObject.SetActive(true);
				}
			}
		}
		yield return new WaitForSeconds(3f);
		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				_playerWrappers[i]._player._uiCamera.enabled = false;
			}
		}

		//yield return StatScreenManager.instance.ShowRoundScoreScreen();
		//yield return new WaitForSeconds(3f);

		for (int i = 0; i < _playerWrappers.Length; i++) {
			if (_playerWrappers[i] != null) {
				_playerWrappers[i]._player.GameOverTasks();
			}
		}
		inGame = false;
		CinematicCamera.instance.ShowCamera(true);
		StatScreenManager.instance.ShowMatchScreen();
	}


	//------------------
	// NETWORK COMMANDS
	//------------------

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
		//base.OnServerAddPlayer(conn, playerControllerId);
		//print("network player");
		GameObject player = SpawnPlayer(0);
		NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
	}

	public GameObject SpawnBall(Vector3 position, NetworkHash128 assetId) {
		//print("spawning " + position);
		GameObject ball = GetBall();
		ball.GetComponent<CannonBall>().CmdSetRenderer(true);
		return ball;
	}

	public void UnSpawnBall(GameObject spawned) {
		//print("despawning");
		spawned.GetComponent<CannonBall>().Disable();
	}






	public static void ShuffleList<T>(ref List<T> list) {
		for (int i = list.Count - 1; i > 0; i--) {
			int rnd = Random.Range(0, i + 1);
			T t = list[i];

			list[i] = list[rnd];
			list[rnd] = t;
		}
	}

	public static void ShuffleArray<T>(ref T[] array) {
		for (int i = array.Length - 1; i > 0; i--) {
			int rnd = Random.Range(0, i + 1);
			T t = array[i];

			array[i] = array[rnd];
			array[rnd] = t;
		}
	}
}
