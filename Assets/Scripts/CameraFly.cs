﻿using UnityEngine;
using System.Collections;

public class CameraFly : MonoBehaviour {

	float speed = 12F;
	
	void Update () {
		if (Input.GetKey(KeyCode.W)) {
			transform.position += transform.forward * Time.deltaTime * speed;
		}
		if (Input.GetKey(KeyCode.S)) {
			transform.position -= transform.forward * Time.deltaTime * speed;
		}
		if (Input.GetKey(KeyCode.A)) {
			transform.position -= transform.right * Time.deltaTime * speed;
		}
		if (Input.GetKey(KeyCode.D)) {
			transform.position += transform.right * Time.deltaTime * speed;
		}
		if (Input.GetKey(KeyCode.Space)) {
			transform.position += Vector3.up * Time.deltaTime * speed;
		}
		if (Input.GetKey(KeyCode.C)) {
			transform.position += Vector3.down * Time.deltaTime * speed;
		}
		// else if (Input.GetKey(KeyCode.Q)) {
			// transform.Rotate(transform.up, Time.deltaTime * 30);
		// }
		// else if (Input.GetKey(KeyCode.E)) {
			// transform.Rotate(Vector3.up, -Time.deltaTime * 30);
		// }
	}
}
