﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Tank : NetworkBehaviour {

	public static float _G = 19.62f;
	
	public enum Type { Light, Medium, Heavy };

	[HideInInspector]
	[SyncVar]
	public int _playerIndex;
	[HideInInspector]
	[SyncVar]
	public string _controlString;
	[HideInInspector]
	public string _playerName;
	[Range(0.1f, 5f)]
	public float _cameraSensitivity = 1f;
	public TankCamera _cameraScript;

	[Header("Parts")]
	public Transform _bottom;
	public Transform _top;
	public Transform _turretPivot;
	public Transform _shotPoint;
	public TrailRenderer _boostTrail;
	public ParticleSystem _boostParticles;
	public Transform _muzzleFlash;
	public ParticleSystem _shotParticles;
	public ParticleSystem _overheatParticles;

	[Header("Other")]
	public AnimationCurve _turningMultp;

	[Header("Prefabs")]
	public Transform _particlePrefab;
	public Transform _projectileArcPrefab;
	public Transform _projectileTargetPrefab;
	public Transform _uiCameraPrefab;
	public Transform _audioSourcesPrefab;
	private ParticleSystem _damageParticles;
	private ParticleSystem[] _showDamageParticles;
	private ParticleSystem _deathParticles;
	private LineRenderer _projectileArc;
	private Transform _targetObject;

	[Header("Graphics")]
	public MeshRenderer _bottomR;
	public MeshRenderer _topR;
	public MeshRenderer _boosterR;
	public MeshRenderer _cannonR;
	public MeshRenderer _wheelsR;
	public MeshRenderer _tracksR;
	private Material _cannonMaterial;
	//public Material _bodyMat;
	//public Material _boosterMat;
	//public Material _turretMat;
	public Material _bodyDamageMat;
	public Material _boosterDamageMat;
	public Material _turretDamageMat;

	private Animator _turretAnimator;
	private Animator _muzzleFlashAnimator;
	private Tank.Type _tankType;

	// Tank stats
	private float _maxDamage;
	private float _maxSpeed = 7f;
	private float _speed = 0f;
	private float _maxHealth = 100f;
	[HideInInspector]
	[SyncVar(hook = "NetworkDamage")]
	public float _health;
	private float _maxBoost = 100f;
	[SyncVar]
	private float _currentBoost;
	private float _maxHeat = 100f;
	private float _currentHeat = 0f;

	private float _turningSpeed = 180f;
	private float _acceleration = 10f;
	private float _deceleration = 7f;
	private float _speedChangeAccelerationMultp = 2f;
	private float _shotCooldown = 0.75f;
	private float _shotCooldownTimer = 0f;
	
		
	private float _startFacing = 0f;
	private float _aimOffset = 35f;
	private float _gravity = _G;
	private Vector3 _gravityVector;
	private float _shotSpeed = 25f;

	private float _boostUsePerSec = 40f;
	private float _boostRechargePerSec = 25f;
	private float _boostRechargeCooldown = 1.5f;
	private float _boostCooldownTimer = 0f;
	private float _boostSpeedMultp = 2f;
	private float _boostSpeedMultpAir = 2f;
	private float _boostAccelerationMultp = 2f;
	private float _currentBoostMultp;
	private float _boostFowMultp = 0.75f;
	private float _currentFowMultp = 0f;
	[SyncVar]
	private bool _boostActive = false;

	private float _heatRechargePerSec = 25f;
	private float _heatRechargeCooldown = 1f;
	private float _heatCooldownTimer = 0f;
	private float _heatAlertThreshold = 0.6f;
	private bool _overheated = false;

	private bool _grounded = false;
	private bool _groundedPreviousFrame = false;
	private Vector3 _collisionVector;
	private float _collisionSpeed;

	public bool _gamepadControls = false;
	public bool _altControlStyle = false;

	private float _verticalInput = 0f;
	private float _horizontalInput = 0f;
	private Vector3 _altMovementVector = new Vector3(0f, 0f, 1f);

	private AudioSource _engineLoopSource;
	private AudioSource _alertLoopSource;
	private AudioSource _turboLoopSource;
	private AudioSource _trackLoopSource;

	// UI
	[HideInInspector]
	public Camera _uiCamera;
	[HideInInspector]
	public Transform loserText;
	[HideInInspector]
	public Transform winnerText;
	[HideInInspector]
	public Transform drawText;
	private Image _healthBar;
	private Image _boostBar;
	private Image _heatBar;
	private Image _heatBarOverlay;
	private Color _heatOverlayColor;
	private float _heatOverlaySine;
	private Animator _hudFlashAnimator;

	[HideInInspector]
	public int _win;
	[HideInInspector]
	public int _kills;
	[HideInInspector]
	public int _suicide;
	[HideInInspector]
	public int _shotsFired;
	[HideInInspector]
	public int _shotsHit;
	[HideInInspector]
	public float _accuracy;
	[HideInInspector]
	public float _damageDealt;
	[HideInInspector]
	public float _selfDamage;

	public void Initialize(int playerIndex, float facing) {
		//print("initialize player");
		_playerIndex = playerIndex;
		_controlString = "C" + GameManager._playerWrappers[playerIndex]._controllerIndex;
		_altControlStyle = StatScreenManager.instance._playerSettings[playerIndex]._altControls;
		_cameraSensitivity = StatScreenManager.instance._playerSettings[playerIndex]._sensitivityMultp;
		_tankType = (Tank.Type)StatScreenManager.instance._playerSettings[playerIndex]._tankIndex;
		_playerName = StatScreenManager.instance._playerData[playerIndex]._playerName;
		_startFacing = facing;
		_bottom.Rotate(Vector3.up, facing);
		_turretAnimator = _cannonR.transform.parent.GetComponent<Animator>();
		_muzzleFlashAnimator = _muzzleFlash.GetComponent<Animator>();
		_cannonMaterial = _cannonR.material;
		_cannonR.material = _cannonMaterial;

		_maxDamage = TankStats.GetStat(_tankType, TankStats.StatType.Damage);
		_maxSpeed = TankStats.GetStat(_tankType, TankStats.StatType.Speed);
		_maxHealth = TankStats.GetStat(_tankType, TankStats.StatType.HitPoints);
		_maxBoost = TankStats.GetStat(_tankType, TankStats.StatType.Turbo);
		_shotCooldown = TankStats.GetStat(_tankType, TankStats.StatType.ShotCooldown);
		_boostRechargeCooldown = TankStats.GetStat(_tankType, TankStats.StatType.TurboCooldown);
		_health = _maxHealth; // Calls NetworkDamage, causes local game to crash in editor
		_currentBoost = _maxBoost;
		if (_tankType == Type.Light) {
			_gravity *= 0.9f;
		}
		else if (_tankType == Type.Heavy) {
			_gravity *= 1.2f;
		}
		
		_bottomR.material.color = GameManager.instance._playerColors[playerIndex] - new Color(0.3F, 0.3F, 0.3F, 0F);
		_topR.material.color = GameManager.instance._playerColors[playerIndex] - new Color(0.3F, 0.3F, 0.3F, 0F);
		_wheelsR.material.color = GameManager.instance._playerColors[playerIndex] - new Color(0.5F, 0.5F, 0.5F, 0F);
		_currentFowMultp = 1f;

		// Instantiate UI camera & set HUD
		_uiCamera = Instantiate(_uiCameraPrefab, transform, false).GetComponent<Camera>();
		_healthBar = _uiCamera.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>();
		_boostBar = _uiCamera.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>();
		_heatBar = _uiCamera.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(2).GetChild(0).GetComponent<Image>();
		_heatBarOverlay = _uiCamera.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(2).GetChild(1).GetComponent<Image>();
		_heatOverlayColor = _heatBarOverlay.color;
		loserText = _uiCamera.transform.GetChild(0).GetChild(2).GetChild(0);
		winnerText = _uiCamera.transform.GetChild(0).GetChild(2).GetChild(1);
		drawText = _uiCamera.transform.GetChild(0).GetChild(2).GetChild(2);
		_hudFlashAnimator = _uiCamera.transform.GetChild(0).GetChild(4).GetComponent<Animator>();
		// Instantiate others
		Transform particleParent = Instantiate(_particlePrefab, transform, false);
		_damageParticles = particleParent.GetChild(0).GetComponent<ParticleSystem>();
		_showDamageParticles = new ParticleSystem[] { particleParent.GetChild(1).GetComponent<ParticleSystem>(), particleParent.GetChild(2).GetComponent<ParticleSystem>() };
		_deathParticles = particleParent.GetChild(3).GetComponent<ParticleSystem>();
		_projectileArc = Instantiate(_projectileArcPrefab, transform, false).transform.GetComponent<LineRenderer>();
		_targetObject = Instantiate(_projectileTargetPrefab, transform, false);
		Transform audioParent = Instantiate(_audioSourcesPrefab, transform, false);
		_engineLoopSource = audioParent.GetChild(0).GetComponent<AudioSource>();
		_alertLoopSource = audioParent.GetChild(1).GetComponent<AudioSource>();
		_turboLoopSource = audioParent.GetChild(2).GetComponent<AudioSource>();
		_trackLoopSource = audioParent.GetChild(3).GetComponent<AudioSource>();

		_projectileArc.gameObject.layer = 11 + _playerIndex;
		_targetObject.GetChild(0).gameObject.layer = 11 + _playerIndex;

		if (GameManager.instance._gameMode == GameManager.GameMode.Local) {
			_cameraScript.Initialize(transform, true, _startFacing, _cameraSensitivity, GameManager.instance._renderMasks[_playerIndex]);
			// Update not called on instantiation frame - manual call to align tank correctly
			Update();
			// Reorient cannon
			Quaternion cameraRot2 = _cameraScript.transform.rotation;
			_top.rotation = Quaternion.Euler(0f, cameraRot2.eulerAngles.y, 0f);
			_turretPivot.rotation = Quaternion.Euler(cameraRot2.eulerAngles.x - _aimOffset, _top.eulerAngles.y, 0f);
		}
	}

	// Has to be in Start: isLocalPlayer is not set in Initialize
	private void Start() {
		//print("Player" + _playerIndex + ": " + isLocalPlayer);
		if (GameManager.instance._gameMode == GameManager.GameMode.Network) {
			_cameraScript.Initialize(transform, isLocalPlayer, _startFacing, _cameraSensitivity, GameManager.instance._renderMasks[_playerIndex]);
			// Update not called on instantiation frame - manual call to align tank correctly
			Update();
			// Reorient cannon
			Quaternion cameraRot2 = _cameraScript.transform.rotation;
			_top.rotation = Quaternion.Euler(0f, cameraRot2.eulerAngles.y, 0f);
			_turretPivot.rotation = Quaternion.Euler(cameraRot2.eulerAngles.x - _aimOffset, _top.eulerAngles.y, 0f);
		}
	}

	private void Update() {
		if (GameManager.instance._gameMode == GameManager.GameMode.Local || isLocalPlayer == true) {
			//print("Player" + _playerIndex + ": Tank update");
			_verticalInput = Input.GetAxisRaw(_controlString + "Vertical");
			_horizontalInput = Input.GetAxisRaw(_controlString + "Horizontal");
			// Boost
			if (Input.GetButton(_controlString + "Fire2") && (_altControlStyle == true || (_altControlStyle == false && _verticalInput >= 0f)) && _currentBoost > 0f &&
				_health > 0f && GameManager.instance.gameOver == false && GameManager.instance.cinematic == false) {
				SetBoost(true);
				_currentBoost -= _boostUsePerSec * Time.deltaTime;
				_boostCooldownTimer = _boostRechargeCooldown;
				if (_grounded == true) {
					_currentBoostMultp = _boostSpeedMultp;
				}
				else {
					_currentBoostMultp = _boostSpeedMultp * _boostSpeedMultpAir;
				}
				_currentFowMultp = Mathf.Clamp(_currentFowMultp - (Time.deltaTime), _boostFowMultp, 1f);
				_cameraScript.AdjustFow(_currentFowMultp);
				if (_currentBoost < 0f) {
					_currentBoost = 0f;
					SetBoost(false);
				}
			}
			else {
				SetBoost(false);
				_boostCooldownTimer -= Time.deltaTime;
				if (_boostCooldownTimer <= 0f && _health > 0f && GameManager.instance.gameOver == false) {
					_currentBoost += _boostRechargePerSec * Time.deltaTime;
					if (_currentBoost > _maxBoost) {
						_currentBoost = _maxBoost;
					}
				}
				_currentBoostMultp = Mathf.Clamp(_currentBoostMultp - (2f * Time.deltaTime), 1f, _boostSpeedMultp);
				_currentFowMultp = Mathf.Clamp(_currentFowMultp + (Time.deltaTime), _boostFowMultp, 1f);
				_cameraScript.AdjustFow(_currentFowMultp);
			}

			// Set speed - Default controls
			float currectAcceleration = _acceleration;
			if (_altControlStyle == false) {
				if (_boostActive == true) {
					_verticalInput = 1f;
					currectAcceleration *= _boostAccelerationMultp;
				}
				if (_grounded == true) {
					if (_verticalInput != 0f && _health > 0f && GameManager.instance.gameOver == false && GameManager.instance.cinematic == false) {
						if (Mathf.Sign(_verticalInput) == Mathf.Sign(_speed)) {
							_speed = Mathf.Clamp(_speed + currectAcceleration * _verticalInput * Time.deltaTime, -_maxSpeed * _currentBoostMultp, _maxSpeed * _currentBoostMultp);
						}
						// Faster speed change from forwards to reverse & vice versa
						else {
							_speed = Mathf.Clamp(_speed + currectAcceleration * _speedChangeAccelerationMultp * _verticalInput * Time.deltaTime, -_maxSpeed * _currentBoostMultp, _maxSpeed * _currentBoostMultp);
						}
					}
					else {
						if (_speed > 0f) {
							_speed = Mathf.Clamp(_speed - _deceleration * Time.deltaTime, 0f, _maxSpeed * _currentBoostMultp);
						}
						else {
							_speed = Mathf.Clamp(_speed + _deceleration * Time.deltaTime, -_maxSpeed * _currentBoostMultp, 0f);
						}
					}
				}
				// Aerial speed
				else if (_boostActive == true) {
					_speed = Mathf.Clamp(_speed + currectAcceleration * _verticalInput * Time.deltaTime, -_maxSpeed * _currentBoostMultp, _maxSpeed * _currentBoostMultp);
				}
			}
			// Set speed - Alt controls
			else {
				Vector3 inputVector = new Vector3(_horizontalInput, 0f, _verticalInput);
				if (_boostActive == true) {
					inputVector.y = 1f;
				}
				if (_grounded == true) {
					if (inputVector.sqrMagnitude != 0f && _health > 0f && GameManager.instance.gameOver == false && GameManager.instance.cinematic == false) {
						_speed = Mathf.Clamp(_speed + currectAcceleration * inputVector.magnitude * Time.deltaTime, -_maxSpeed * _currentBoostMultp, _maxSpeed * _currentBoostMultp);
					}
					else {
						_speed = Mathf.Clamp(_speed - _deceleration * Time.deltaTime, 0f, _maxSpeed * _currentBoostMultp);
					}
				}
				else {
					_speed = Mathf.Clamp(_speed + currectAcceleration * inputVector.magnitude * Time.deltaTime, -_maxSpeed * _currentBoostMultp, _maxSpeed * _currentBoostMultp);
				}
			}
			float difference = (1 - _boostFowMultp);
			_turboLoopSource.pitch = 0.5f + Mathf.Clamp01((difference - (_currentFowMultp - _boostFowMultp)) / difference) * 0.5f;
			_turboLoopSource.volume = ((difference - (_currentFowMultp - _boostFowMultp)) / difference) * 0.2f;
			

			if (_health > 0f && GameManager.instance.gameOver == false && GameManager.instance.cinematic == false) {
				// Rotate body - Default controls
				if (_altControlStyle == false) {
					if (_grounded == true) {
						_bottom.Rotate(_bottom.up, _turningSpeed * _turningMultp.Evaluate(Mathf.Abs(_speed) / _maxSpeed) * _horizontalInput * Mathf.Sign(_verticalInput) * Time.deltaTime);
					}
					else {
						_bottom.Rotate(_bottom.up, _turningSpeed * _turningMultp.Evaluate(Mathf.Abs(_speed) / _maxSpeed) * 0.2f * _horizontalInput * Mathf.Sign(_verticalInput) * Time.deltaTime);
					}
				}
				// Rotate body - Alt controls
				else {
					float turning = _turningSpeed;
					if (_grounded == false) {
						turning *= 0.2f;
					}
					Vector3 inputVector = new Vector3(_horizontalInput, 0f, _verticalInput);
					if (inputVector.sqrMagnitude != 0f) {
						_altMovementVector = Vector3.RotateTowards(_altMovementVector, inputVector, turning * _turningMultp.Evaluate(_speed / _maxSpeed) * Time.deltaTime, 0f);
					}
					if (_speed != 0f) {
						Quaternion cameraRot1 = _cameraScript.transform.rotation;
						Quaternion targetRot = Quaternion.Euler(0f, cameraRot1.eulerAngles.y, 0f) * Quaternion.LookRotation(_altMovementVector);
						_bottom.rotation = Quaternion.RotateTowards(_bottom.rotation, Quaternion.Euler(_bottom.rotation.eulerAngles.x, targetRot.eulerAngles.y, _bottom.rotation.eulerAngles.z), turning * _turningMultp.Evaluate(_speed / _maxSpeed) * Time.deltaTime);
					}
				}

				// Rotate turret
				Quaternion cameraRot2 = _cameraScript.transform.rotation;
				_projectileArc.transform.rotation = Quaternion.Euler(Vector3.zero);
				_top.rotation = Quaternion.Euler(0f, cameraRot2.eulerAngles.y, 0f);
				_turretPivot.rotation = Quaternion.Euler(cameraRot2.eulerAngles.x - _aimOffset, _top.eulerAngles.y, 0f);
			}

			// Move forward & gravity
			transform.Translate(((_bottom.forward * _speed) + _gravityVector + (_collisionVector * _collisionSpeed)) * Time.deltaTime, Space.World);
			// Move from collision
			_collisionSpeed = Mathf.Clamp(_collisionSpeed - _deceleration * 2f * Time.deltaTime, 0f, _maxSpeed);

			// Limit position to level bounds
			if (transform.position.x < 1f) {
				transform.position = new Vector3(1f, transform.position.y, transform.position.z);
			}
			else if (transform.position.x > 63f) {
				transform.position = new Vector3(63f, transform.position.y, transform.position.z);
			}
			if (transform.position.z < 1f) {
				transform.position = new Vector3(transform.position.x, transform.position.y, 1f);
			}
			if (transform.position.z > 63f) {
				transform.position = new Vector3(transform.position.x, transform.position.y, 63f);
			}

			// Clamp to ground
			float groundHeight = CreateLevel.instance.GetGroundHeight(transform.position);
			float groundSafetyMargin = 0.01F;
			_groundedPreviousFrame = _grounded;
			if (transform.position.y <= groundHeight + groundSafetyMargin) {
				transform.position = new Vector3(transform.position.x, groundHeight, transform.position.z);
				_grounded = true;
			}
			else {
				_grounded = false;
			}
			// Landing & bottom alignment
			if (_grounded == true) {
				// Play landing sound & add bounce
				if (_groundedPreviousFrame == false) {
					GameManager.instance.PlaySound(GameManager.instance._groundImpactSound, -_gravityVector.y / (_G / 2f) * 0.3f);
					if (-_gravityVector.y < _G / 2f) {
						_gravityVector = Vector3.zero;
					}
					else {
						_gravityVector *= -0.2f;
					}
				}
				else {
					_gravityVector = Vector3.zero;
				}
				// Alight bottom with ground
				if (_groundedPreviousFrame == false || CreateLevel.instance.GetDistanceToGround(transform.position + _bottom.forward * 0.75f) < 0.2f) {
					float frontHeight = CreateLevel.instance.GetGroundHeight(transform.position + _bottom.forward * 0.75F);
					float backHeight = CreateLevel.instance.GetGroundHeight(transform.position - _bottom.forward * 0.75F);
					float rightHeight = CreateLevel.instance.GetGroundHeight(transform.position + _bottom.right * 0.5F);
					float leftHeight = CreateLevel.instance.GetGroundHeight(transform.position - _bottom.right * 0.5F);
					float pitchAngle = -Mathf.Atan((frontHeight - backHeight) / 1.5F) * Mathf.Rad2Deg;
					float rollAngle = Mathf.Atan((rightHeight - leftHeight) / 1.5F) * Mathf.Rad2Deg;
					// Debug.Log("frameCount: " + Time.frameCount + ", pitchAngle: " + pitchAngle + ", rollAngle" + rollAngle);
					// yawAngle = current yaw
					_bottom.rotation = Quaternion.Euler(pitchAngle, _bottom.rotation.eulerAngles.y, rollAngle);
					//_top.rotation = Quaternion.Euler(pitchAngle, _bottom.rotation.eulerAngles.y, rollAngle);
				}
			}
			// Apply gravity when in air
			else {
				_gravityVector -= Vector3.up * _gravity * Time.deltaTime;
				if (_boostActive == true) {
					_bottom.rotation = Quaternion.RotateTowards(_bottom.rotation, Quaternion.Euler(0f, _bottom.rotation.eulerAngles.y, 0f), 10f * Time.deltaTime);
				}
				else {
					_bottom.rotation = Quaternion.RotateTowards(_bottom.rotation, Quaternion.Euler(0f, _bottom.rotation.eulerAngles.y, 0f), 50f * Time.deltaTime);
				}
			}

			DrawProjectileArc();
			Fire();
			
			// Loop sounds
			if (_grounded == true) {
				_engineLoopSource.volume = 0.5f / GameManager._alivePlayers;
				_engineLoopSource.pitch = 1f + ((_speed / _maxSpeed) * 0.5f);
				_trackLoopSource.volume = (_speed / _maxSpeed) * 0.5f / GameManager._alivePlayers;
				_trackLoopSource.pitch = 0.5f + (Mathf.Clamp01(_speed / _maxSpeed) * 0.5f);
			}
			else {
				_engineLoopSource.pitch = 1f + (_speed / _maxSpeed);
				_trackLoopSource.volume = 0f;
			}

			UpdateBoostBar();
			UpdateHeat();
		}
		// For network and local
		LocalAndNetworkVisuals();
	}

	//private void LateUpdate() {
	//	Vector3 uiRot = new Vector3();
	//	uiRot = _bottom.rotation.eulerAngles;
	//	if (uiRot.x > 180) {
	//		uiRot.x -= 360f;
	//	}
	//	if (uiRot.z > 180) {
	//		uiRot.z -= 360f;
	//	}
	//	uiRot.y = 0f;
	//	uiRot /= 10f;
	//	_uiCamera.transform.GetChild(0).GetChild(0).rotation = Quaternion.Euler(uiRot);
	//}

	private void LocalAndNetworkVisuals() {
		if (_boostActive == true) {
			_boostTrail.time = 1f;
			if (_boostParticles.isPlaying == false) {
				_boostParticles.Play();
			}
		}
		else {
			_boostTrail.time -= Mathf.Clamp01(5f * Time.deltaTime);
			_boostParticles.Stop();
		}
	}

	private void OnCollisionEnter(Collision collision) {
		if (collision.collider.tag == "Player") {
			collision.collider.GetComponent<Tank>().PlayerCollision(_bottom.forward, _speed);
			_speed = 0f;
		}
	}

	public void PlayerCollision(Vector3 direction, float speed) {
		_collisionVector = direction.normalized;
		_collisionSpeed = speed;
	}

	public float TakeDamage(float damage) {
		if ((GameManager.instance._gameMode == GameManager.GameMode.Local || isServer == true) && _health > 0f) {
			float returnValue = damage;
			if (damage > _health) {
				returnValue = _health;
			}
			//print("local: " + _playerIndex);
			_health -= damage;
			GameManager.instance.PlaySound(GameManager.instance._playerHitSound, 0.5f);
			_hudFlashAnimator.Play("Flash");
			// Normal damage
			if (_health > 0f) {
				_damageParticles.Play();
				if (_health / _maxHealth <= 0.6f && _showDamageParticles[0].isPlaying == false) {
					_showDamageParticles[0].Play();
				}
				if (_health / _maxHealth <= 0.3f && _showDamageParticles[1].isPlaying == false) {
					_showDamageParticles[1].Play();
				}

			}
			// Death
			else {
				_health = 0f;
				GameManager.instance.PlaySound(GameManager.instance._playerDeathSound, 1f);
				_showDamageParticles[0].Stop();
				_showDamageParticles[1].Stop();
				_deathParticles.Play();
				_bottomR.material.color = GameManager.instance._playerColors[_playerIndex] - new Color(0.7F, 0.7F, 0.7F, 0F);
				_topR.material.color = GameManager.instance._playerColors[_playerIndex] - new Color(0.7F, 0.7F, 0.7F, 0F);
				_wheelsR.material.color = GameManager.instance._playerColors[_playerIndex] - new Color(0.9F, 0.9F, 0.9F, 0F);
				_cannonR.material.color = new Color(0.1F, 0.1F, 0.1F, 1F);
				_tracksR.material.color = new Color(0.1F, 0.1F, 0.1F, 1F);

				_healthBar.transform.parent.parent.gameObject.SetActive(false);
				ShowHUDIcon(0);
				_projectileArc.gameObject.SetActive(false);
				_targetObject.gameObject.SetActive(false);
				StopAudio();
				GameManager.instance.PlayerDeath();
			}
			UpdateHealthBar(_health / _maxHealth);
			return returnValue;
		}
		else {
			return 0f;
		}
	}

	public void ShowHUDIcon(int index) {
		_uiCamera.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
		_uiCamera.transform.GetChild(0).GetChild(0).GetChild(index).gameObject.SetActive(true);
	}
	
	private void NetworkDamage(float newHealth) {
		print("asd " + Time.frameCount);
		//if (GameManager.instance._gameMode == GameManager.GameMode.Network && isServer == false && newHealth != _maxHealth) {
		_health = newHealth;
			print("network " + _playerIndex + ": - " + _health + " - " + Time.frameCount);
			if (_health > 0f) {
				_damageParticles.Play();
				if (_health / _maxHealth <= 0.6f && _showDamageParticles[0].isPlaying == false) {
					_showDamageParticles[0].Play();
				}
				if (_health / _maxHealth <= 0.3f && _showDamageParticles[1].isPlaying == false) {
					_showDamageParticles[1].Play();
				}
			}
			else {
				_showDamageParticles[0].Stop();
				_showDamageParticles[1].Stop();
				_deathParticles.Play();
				_bottomR.material = _bodyDamageMat;
				_topR.material = _bodyDamageMat;
				_cannonR.material = _turretDamageMat;
				_boosterR.material = _boosterDamageMat;
			}
			UpdateHealthBar(_health / _maxHealth);
		//}
	}

	public void UpdateHealthBar(float healthPercentage) {
		_healthBar.fillAmount = healthPercentage;
		_healthBar.color = GameManager.instance._healthGradient.Evaluate(healthPercentage);
	}

	public void UpdateBoostBar() {
		_boostBar.fillAmount = _currentBoost / _maxBoost;
	}

	public void UpdateHeat() {
		_heatCooldownTimer -= Time.deltaTime;
		if (_heatCooldownTimer <= 0f) {
			_currentHeat -= _heatRechargePerSec * Time.deltaTime;
			if (_currentHeat <= 0f) {
				_currentHeat = 0f;
				if (_overheated == true) {
					_overheatParticles.Stop();
				}
				_overheated = false;
			}
		}
		_heatBar.fillAmount = (_currentHeat / _maxHeat);
		if (_overheated == true) {
			_heatOverlayColor.a = _heatBar.fillAmount;
			_cannonMaterial.SetFloat("_RimPower", GameManager.instance._heatCannonCurve.Evaluate(_heatBar.fillAmount / 0.1f) * 3f);
			_alertLoopSource.volume = 0f;
		}
		else {
			_heatOverlaySine += Time.deltaTime * (1f + (GameManager.instance._heatOverlayCurve.Evaluate(_heatBar.fillAmount) * 2f));
			if (_heatOverlaySine >= 1f) {
				_heatOverlaySine -= 1f;
			}
			_heatOverlayColor.a = GameManager.instance._heatOverlayCurve.Evaluate(_heatBar.fillAmount * Mathf.Sin(_heatOverlaySine * 3.14f));
			_cannonMaterial.SetFloat("_RimPower", GameManager.instance._heatCannonCurve.Evaluate(_heatBar.fillAmount) * 3f);
			if (_heatBar.fillAmount > _heatAlertThreshold) {
				_alertLoopSource.volume = (_heatBar.fillAmount - _heatAlertThreshold) / _heatAlertThreshold * 0.4f;
			}
			else {
				_alertLoopSource.volume = 0f;
			}
		}

		_heatBarOverlay.color = _heatOverlayColor;
	}

	private void DrawProjectileArc() {
		float startH = _shotPoint.position.y;
		float angle = (_turretPivot.rotation.eulerAngles.x * -1f) * Mathf.Deg2Rad;
		float x = 0f;
		float arcH = 0f;
		Vector3 forward = _top.forward;
		Vector3 vect = Vector3.zero;
		List<Vector3> arcPointList = new List<Vector3>();
		for (int i = 0; i < 50; i++) {
			x = i * 2f;
			arcH = startH + x * Mathf.Tan(angle) - ((_G * x * x) / (2 * (_shotSpeed * _shotSpeed * Mathf.Cos(angle) * Mathf.Cos(angle))));
			vect = _shotPoint.position + (forward * x);
			float groundH = CreateLevel.instance.GetGroundHeight(vect);
			if (arcH < groundH) {
				float minX = (i - 1) * 2f;
				float maxX = x;

				for (int j = 0; j < 8; j++) {
					x = (maxX - minX) / 2f + minX;
					arcH = startH + x * Mathf.Tan(angle) - ((_G * x * x) / (2 * (_shotSpeed * _shotSpeed * Mathf.Cos(angle) * Mathf.Cos(angle))));
					vect = _shotPoint.position + (forward * x);
					groundH = CreateLevel.instance.GetGroundHeight(vect);
					if (arcH < groundH) {
						maxX = x;
					}
					else {
						arcPointList.Add(vect + Vector3.up * arcH);
						minX = x;
					}
				}
				break;
			}
			else {
				arcPointList.Add(vect + Vector3.up * arcH);
			}
			if (vect.x < 0f || vect.x > 63f || vect.z < 1f || vect.z > 63f) {
				break;
			}
			if (i == 49) {
				Debug.LogWarning("Not enough points for drawing projectile arc!");
			}
		}
		_projectileArc.positionCount = arcPointList.Count;
		Vector3[] pointArray = new Vector3[arcPointList.Count];
		for (int i = 0; i < arcPointList.Count; i++) {
			pointArray[i] = arcPointList[i] - Vector3.up * _shotPoint.position.y;
		}
		_projectileArc.SetPositions(pointArray);
		_targetObject.position = new Vector3(vect.x, CreateLevel.instance.GetGroundHeight(vect), vect.z);
	}

	private IEnumerator MuzzleFlashRoutine() {
		//_muzzleFlash.Rotate(Vector3.up, Random.Range(0, 180f));
		_shotParticles.Play();
		_muzzleFlashAnimator.Play("Shoot");
		yield return null;
	}


	//------------------
	// NETWORK COMMANDS
	//------------------
	
	private void SetBoost(bool boost) {
		if (_boostActive != boost) {
			_boostActive = boost;
			if (GameManager.instance._gameMode == GameManager.GameMode.Network) {
				CmdSetBoost(boost);
			}
		}
	}

	[Command]
	void CmdSetBoost(bool boost) {
		_boostActive = boost;
	}

	private void Fire() {
		if (_health > 0f && GameManager.instance.gameOver == false && GameManager.instance.paused == false && GameManager.instance.cinematic == false) {
			_shotCooldownTimer -= Time.deltaTime;
			if ((Input.GetButtonDown(_controlString + "Fire1") || Input.GetAxisRaw(_controlString + "Fire1") < 0f) && _shotCooldownTimer <= 0f && _overheated == false) {
				_shotsFired++;
				if (GameManager.instance._gameMode == GameManager.GameMode.Network) {
					CmdFire();
				}
				else {
					LocalFire();
				}
				_heatCooldownTimer = _heatRechargeCooldown;
				_currentHeat += _maxDamage / 3f;
				if (_currentHeat >= _maxHeat) {
					_currentHeat = _maxHeat;
					_heatCooldownTimer = _heatRechargeCooldown;
					_overheated = true;
					_overheatParticles.Play();
					GameManager.instance.PlaySound(GameManager.instance._overheatSound, 1f);
				}
			}
		}
	}
	
	[Command]
	void CmdFire() {
		//print("Player" + _playerIndex + ": shooting");
		_shotCooldownTimer = _shotCooldown;
		GameManager.instance.PlaySound(GameManager.instance._shootSound, 1f);
		GameObject ball = GameManager.instance.GetBall();
		ball.GetComponent<CannonBall>().Enable(transform, _shotPoint.position, _turretPivot.forward * _shotSpeed, _maxDamage, _tankType);
		//_cameraScript.CameraShake(0.1f, 0.3f);
		_turretAnimator.Play("Shoot");
		StartCoroutine(MuzzleFlashRoutine());

		NetworkServer.Spawn(ball, GameManager.instance._ballAssetId);
	}
	
	void LocalFire() {
		//print("Player" + _playerIndex + ": shooting");
		_shotCooldownTimer = _shotCooldown;
		GameManager.instance.PlaySound(GameManager.instance._shootSound, 1f);
		GameObject ball = GameManager.instance.GetBall();
		ball.GetComponent<CannonBall>().Enable(transform, _shotPoint.position, _turretPivot.forward * _shotSpeed, _maxDamage, _tankType);
		_cameraScript.CameraShake(0.1f, 0.3f);
		_turretAnimator.Play("Shoot");
		StartCoroutine(MuzzleFlashRoutine());
	}

	public void ShotHit(float damage, int kills) {
		_shotsHit++;
		_damageDealt += damage;
		_kills += kills;
	}

	public void Pause(bool b) {
		if (b == true) {
			_engineLoopSource.Pause();
			_alertLoopSource.Pause();
			_turboLoopSource.Pause();
			_trackLoopSource.Pause();
		}
		else {
			_engineLoopSource.UnPause();
			_alertLoopSource.UnPause();
			_turboLoopSource.UnPause();
			_trackLoopSource.UnPause();
		}
	}

	public void GameOverTasks() {
		_cameraScript._camera.enabled = false;
		//_uiCamera.enabled = false;
		_projectileArc.gameObject.SetActive(false);
		_targetObject.gameObject.SetActive(false);
		StopAudio();
		if (_shotsFired > 0) {
			_accuracy = 1f * _shotsHit / _shotsFired;
		}
		else {
			_accuracy = 0f;
		}
	}

	private void StopAudio() {
		_engineLoopSource.Stop();
		_alertLoopSource.Stop();
		_turboLoopSource.Stop();
		_trackLoopSource.Stop();
	}
}
