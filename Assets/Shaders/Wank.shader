Shader "Wank/Diffuse" {
Properties {
	_MainTex ("Grass (RGB)", 2D) = "white" {}
	_RockTex ("Rock (RGB)", 2D) = "white" {}
	_AlphaTex ("Alpha", 2D) = "white" {}
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 200

CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
sampler2D _RockTex;
sampler2D _AlphaTex;

struct Input {
	float2 uv_MainTex;
	float2 uv_AlphaTex;
	float2 uv_RockTex;
};

void surf (Input IN, inout SurfaceOutput o) {
	float2 offset = float2(0.004, 0.007);
	float2 offset2 = float2(-0.004, 0.007);
	float alpha = (tex2D(_AlphaTex, IN.uv_AlphaTex).r + tex2D(_AlphaTex, IN.uv_AlphaTex + offset).r + tex2D(_AlphaTex, IN.uv_AlphaTex - offset).r + tex2D(_AlphaTex, IN.uv_AlphaTex + offset2).r + tex2D(_AlphaTex, IN.uv_AlphaTex - offset2).r) / 5;
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * alpha + tex2D(_RockTex, IN.uv_RockTex) * 0.1 * (1 - alpha);
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}
ENDCG
}

Fallback "Legacy Shaders/VertexLit"
}
